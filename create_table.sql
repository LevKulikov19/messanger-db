﻿CREATE TABLE Role (
    role_id TINYINT PRIMARY KEY NOT NULL IDENTITY,
    name VARCHAR(100) NOT NULL UNIQUE CHECK (LEN(name)<=100),
    description VARCHAR(1024) NOT NULL UNIQUE CHECK (LEN(description)<=1024)
);

CREATE TABLE Interface_language(
    interface_language_id TINYINT PRIMARY KEY NOT NULL IDENTITY,
    name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE App_theme (
    app_theme_id TINYINT PRIMARY KEY NOT NULL IDENTITY,
    name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE Notification_sound_settings (
    notification_sound_settings_id TINYINT PRIMARY KEY NOT NULL IDENTITY,
    name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE User_settings (
    user_settings_id SMALLINT PRIMARY KEY NOT NULL IDENTITY,
    notification_sound_settings_id TINYINT NOT NULL REFERENCES Notification_sound_settings (notification_sound_settings_id),
    app_theme_id TINYINT NOT NULL REFERENCES App_theme (app_theme_id),
    interface_language_id TINYINT NOT NULL REFERENCES Interface_language (interface_language_id)
);

CREATE TABLE User_main (
    user_id INT PRIMARY KEY NOT NULL IDENTITY,
    last_name VARCHAR(64) NOT NULL CHECK (LEN(last_name) BETWEEN 2 AND 64),
    first_name VARCHAR(64) CHECK (LEN(first_name) BETWEEN 2 AND 64),
    middle_name VARCHAR(64) CHECK (LEN(middle_name) BETWEEN 2 AND 64),
    photo VARCHAR(1024) UNIQUE CHECK (photo LIKE 'https://[a-z0-9]%.[a-z0-9]%'),
    nickname VARCHAR(255) UNIQUE CHECK (LEN(nickname) BETWEEN 5 AND 32 AND nickname LIKE '%[^!-~]%') DEFAULT LEFT(CONVERT(VARCHAR(255), NEWID()), 32),
    online_status BIT NOT NULL,
    phone_number VARCHAR(15) UNIQUE CHECK (phone_number LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    registration_date DATE DEFAULT GETDATE(),
	description VARCHAR(70),
    user_settings_id SMALLINT NOT NULL REFERENCES User_settings(user_settings_id),
    role_id TINYINT NOT NULL REFERENCES Role(role_id),
	is_delete BIT NOT NULL DEFAULT 0
);

CREATE TABLE User_verify_code (
	user_id INT PRIMARY KEY NOT NULL REFERENCES User_main(user_id),
	verify_code DECIMAL(6,0)
);

CREATE TABLE Notifications (
    notification_id BIGINT PRIMARY KEY NOT NULL IDENTITY,
    send_time DATETIME NOT NULL,
    notification_text VARCHAR(MAX) NOT NULL CHECK (LEN(notification_text) BETWEEN 1 AND 255),
    user_id INT NOT NULL REFERENCES User_main(user_id)
);

CREATE TABLE User_account_access (
    operation_id INT PRIMARY KEY NOT NULL IDENTITY,
    date_time DATETIME NOT NULL,
    operation BIT NOT NULL,
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES User_main(user_id)
);

CREATE TABLE User_contacts (
    user_recorded_name VARCHAR(255) NOT NULL,
    blocked BIT NOT NULL,
	user_contact_id INT NOT NULL,
    user_id INT,
    PRIMARY KEY (user_contact_id, user_id),
    FOREIGN KEY (user_contact_id) REFERENCES User_main(user_id),
    FOREIGN KEY (user_id) REFERENCES User_main(user_id)
);

CREATE TABLE User_contacts_not_registered_in_app (
    unregistered_user_contact_id INT PRIMARY KEY NOT NULL IDENTITY,
    user_contact_phone_number VARCHAR(15) NOT NULL CHECK (LEN(user_contact_phone_number)=11),
    user_recorded_name VARCHAR(255) NOT NULL
);

CREATE TABLE Message_type (
    message_type_id TINYINT PRIMARY KEY NOT NULL IDENTITY,
    name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE Sticker (
    sticker_id INT PRIMARY KEY NOT NULL IDENTITY,
    image VARCHAR(1024) NOT NULL UNIQUE CHECK (image LIKE 'https://[a-z0-9]%.[a-z0-9]%'),
    emoji VARCHAR(255) NOT NULL,
	user_create INT NOT NULL REFERENCES User_main(user_id),
);

CREATE TABLE Chat_messages (
    chat_message_id BIGINT PRIMARY KEY NOT NULL IDENTITY,
    send_time DATETIME NOT NULL,
    edit_time DATETIME,
	user_create INT NOT NULL REFERENCES User_main(user_id),
		
    message_type_id TINYINT NOT NULL REFERENCES Message_type(message_type_id),
    message_id BIGINT NOT NULL,

	is_private_chat BIT NOT NULL,
	chat_id INT NOT NULL,

	is_delete BIT NOT NULL DEFAULT 0,
	is_moderated BIT NOT NULL DEFAULT 0,
);

CREATE TABLE Sticker_in_Chat_messages (
	sticker_in_chat_messages_id BIGINT PRIMARY KEY IDENTITY,
	chat_message_id BIGINT REFERENCES Chat_messages (chat_message_id),
	sticker_id INT REFERENCES Sticker(sticker_id),
	caption VARCHAR(255),
)

CREATE TABLE Text_message (
    text_message_id BIGINT PRIMARY KEY NOT NULL IDENTITY,
    message_text VARCHAR(4096) NOT NULL,
	chat_message_id BIGINT REFERENCES Chat_messages (chat_message_id)
);

CREATE TABLE File_group (
    file_group_id BIGINT PRIMARY KEY NOT NULL IDENTITY,
    caption VARCHAR(4096),
	chat_message_id BIGINT REFERENCES Chat_messages (chat_message_id)
)

CREATE TABLE File_main (
    file_id BIGINT PRIMARY KEY NOT NULL IDENTITY,
    file_link VARCHAR(1024) NOT NULL UNIQUE CHECK (file_link LIKE 'https://[a-z0-9]%.[a-z0-9]%'),
    size INT CHECK (LEN(size)<= 1048576), -- размер указан в байтах
	file_group_id BIGINT REFERENCES File_group (file_group_id)
);

CREATE TABLE User_downloaded_file (
    download_id INT PRIMARY KEY NOT NULL IDENTITY,
    download_time DATETIME NOT NULL,
    downloading_user_id INT NOT NULL REFERENCES User_main(user_id),
    file_id BIGINT NOT NULL REFERENCES File_main (file_id),
);

CREATE TABLE Private_chat (
    private_chat_id INT PRIMARY KEY NOT NULL IDENTITY,
	pin_message BIGINT REFERENCES Chat_messages(chat_message_id),
    user_1_id INT NOT NULL REFERENCES User_main(user_id),
    user_2_id INT NOT NULL REFERENCES User_main(user_id)
);

CREATE TABLE Group_chat (
    group_chat_id INT PRIMARY KEY NOT NULL IDENTITY,
	pin_message BIGINT REFERENCES Chat_messages(chat_message_id),
    name VARCHAR(255) NOT NULL,
    creator_user_id INT NOT NULL,
	is_delete BIT NOT NULL DEFAULT 0,
    FOREIGN KEY (creator_user_id) REFERENCES User_main(user_id)
);

CREATE TABLE Private_chat_Chat_messages (
	chat_message_id BIGINT REFERENCES Chat_messages (chat_message_id),
	private_chat_id INT REFERENCES Private_chat (private_chat_id),
	PRIMARY KEY (chat_message_id, private_chat_id)
);

CREATE TABLE Group_chat_Chat_messages (
	chat_message_id BIGINT REFERENCES Chat_messages (chat_message_id),
	group_chat_id INT REFERENCES Group_chat (group_chat_id),
	PRIMARY KEY (chat_message_id, group_chat_id)
);

CREATE TABLE Block_type (
    block_type_id TINYINT PRIMARY KEY NOT NULL IDENTITY,
    name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE Blocked_user (
    block_id INT PRIMARY KEY NOT NULL IDENTITY,
    block_time_in_days INT NOT NULL,
    blocked_user_id INT NOT NULL REFERENCES User_main(user_id),
    blocking_user_id INT NOT NULL REFERENCES User_main(user_id),
	block_type_id TINYINT NOT NULL REFERENCES Block_type(block_type_id),
	block_date DATETIME NOT NULL
);

CREATE TABLE Group_chat_participants (
    group_chat_id INT NOT NULL,
    user_id INT,
    PRIMARY KEY (group_chat_id, user_id),
    FOREIGN KEY (group_chat_id) REFERENCES Group_chat(group_chat_id),
    FOREIGN KEY (user_id) REFERENCES User_main(user_id)
);

CREATE TABLE Phonebook_contacts_not_registered_in_app (
    user_id INT NOT NULL REFERENCES User_main(user_id),
    unregistered_user_contact_id INT NOT NULL
		REFERENCES User_contacts_not_registered_in_app(unregistered_user_contact_id),
	PRIMARY KEY (unregistered_user_contact_id, user_id)
);

CREATE TABLE Messages_read (
    user_id INT NOT NULL REFERENCES User_main(user_id),
    chat_message_id BIGINT NOT NULL REFERENCES Chat_messages(chat_message_id),
    PRIMARY KEY (chat_message_id, user_id)
);

CREATE TABLE Users_statistics_for_app (
    statistics_collection_id INT PRIMARY KEY NOT NULL IDENTITY,
    number_of_sent_messages INT NOT NULL,
    number_of_sent_files INT NOT NULL,
    total_volume_of_sent_files FLOAT NOT NULL, -- в ГБ
    number_of_downloads_of_sent_files INT NOT NULL,
    number_of_sent_stickers INT NOT NULL,
    user_id INT NOT NULL REFERENCES User_main(user_id),
	date_collected DATETIME NOT NULL 
);
