﻿--последовательность не менять
INSERT INTO Role (name, description) 
VALUES 
('Пользователь', 'Создает сообщения в чатах. Добавляет стикеры в чатах. Изменяет свои данные профиля как пользователя. Изменяет данные визуальной настройки приложения для себя. Добавляет участников в групповой чат. Добавляет закрепленные сообщения в групповом чате. Изменяет для себя отображение имени у контактов'), 
('Модератор чатов', 'Просматривает все сообщения для поиска запрещенных законом выражений. Запретить вход пользователю на некоторое время'), 
('Создатель стикеров', 'Создавает новые стикеры для чатов и редактирует стикеры которые уже создал.'),
('Администратор', 'Формирует аналитику по мессенджеру. Решает конфликтные ситуации у модераторов.')

INSERT INTO Interface_language (name) 
VALUES ('Русский'), ('Английский')

INSERT INTO App_theme (name) 
VALUES ('Светлая'), ('Темная')

INSERT INTO Notification_sound_settings (name) 
VALUES ('выключено'), ('только личные сообщения'), ('только сообщения из групп'), ('включено')

INSERT INTO User_settings (notification_sound_settings_id, app_theme_id, interface_language_id) 
VALUES (1, 1, 1), (1, 2, 1), (1, 1, 1), (2, 2, 1), (1, 1, 2), (1, 2, 2), (1, 1, 2), (2, 2, 2)

DECLARE @count_reader INT
SET @count_reader = 1000;
DECLARE @couter INT
SET @couter = 0;
WHILE @couter < @count_reader
BEGIN
	INSERT INTO User_main
	(last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id)
	VALUES
	('Алексеев', 'Алексей', 'Алексеевич', 'https://example.com/photo' + CAST(@couter AS varchar(100)) + '.jpg', 'Alekseev' + CAST(@couter AS varchar(100)), RAND(), CAST(70000000000 + @couter AS varchar(15)), DATEADD(DAY, RAND() * DATEDIFF(DAY, '2020-11-12 10:28:51.000', '2023-11-12 10:28:51.000'), '2020-11-12 10:28:51.000'), ')', FLOOR(1 + (RAND() * (8 - 1 + 1))), 4)
	SET @couter = @couter + 1; 
END

--DELETE FROM User_main

--DECLARE @count_reader INT
SET @count_reader = 1000;
--DECLARE @couter INT
SET @couter = 1;
WHILE @couter < @count_reader
BEGIN
INSERT INTO Notifications (send_time, notification_text, user_id) 
VALUES (DATEADD(DAY, RAND() * DATEDIFF(DAY, '2020-11-12 10:28:51.000', '2023-11-12 10:28:51.000'), (SELECT registration_date FROM User_main WHERE User_main.user_id = @couter)), 'Привет' + CAST(@couter AS varchar(100)), @couter)
SET @couter = @couter + 1;
END


--DELETE FROM Notifications

SET @count_reader = 60;

SET @couter = 1;
WHILE @couter < @count_reader
BEGIN
INSERT INTO User_account_access 
(date_time, operation, user_id) 
VALUES 
(DATEADD(MINUTE, -@couter, GETDATE()), 1, @couter), (DATEADD(MINUTE, -@couter, GETDATE()), 0, @couter)
SET @couter = @couter + 1; 
END



--DELETE FROM User_account_access

INSERT INTO User_contacts 
(user_recorded_name, blocked, user_id) 
VALUES 
('Алексей', 0, 1), ('Ваня', 0, 2)

INSERT INTO User_contacts_not_registered_in_app 
(user_contact_phone_number, user_recorded_name) 
VALUES 
('89065838796', 'Брат'), ('89507405217', 'Мама'), ('89104783215','Сестра'), ('89104783215', 'Неизвестный')

GO


DECLARE @count_reader INT
SET @count_reader = 1000;
DECLARE @couter INT
SET @couter = 1;
WHILE @couter < @count_reader
BEGIN
	INSERT INTO Private_chat 
	(user_1_id, user_2_id) 
	VALUES 
	(@couter, @couter+1)
	SET @couter = @couter + 1; 
END

GO

DECLARE @count_reader INT
SET @count_reader = 20;
DECLARE @couter INT
SET @couter = 5;
WHILE @couter < @count_reader
BEGIN
	INSERT INTO Private_chat 
	(user_1_id, user_2_id) 
	VALUES 
	(1, @couter)
	SET @couter = @couter + 1; 
END

GO

DECLARE @count_reader INT
SET @count_reader = 20;
DECLARE @couter INT
SET @couter = 5;
WHILE @couter < @count_reader
BEGIN
	INSERT INTO Private_chat 
	(user_1_id, user_2_id) 
	VALUES 
	(3, @couter)
	SET @couter = @couter + 1; 
END

GO

INSERT INTO Group_chat 
(name, creator_user_id) 
VALUES 
('Чат для работы', 2), ('Семейный чат', 1)

INSERT INTO Message_type 
(name) 
VALUES 
('Текстовое сообщение'),
('Стикер'),
('Файлы'),
('Пересланное сообщение')

INSERT INTO Sticker 
(image, emoji, user_create) 
VALUES 
('https://sticker1.png', '\xF0\x9F\x98\x81', 1),
('https://sticker2.png', '\xF0\x9F\x98\x82', 1),
('https://sticker3.png', '\xF0\x9F\x98\x83', 1),
('https://sticker4.png', '\xF0\x9F\x98\x84', 1),
('https://sticker5.png', '\xF0\x9F\x98\x85', 1),
('https://sticker6.png', '\xF0\x9F\x98\x86', 1),
('https://sticker7.png', '\xF0\x9F\x98\x87', 1),
('https://sticker8.png', '\xF0\x9F\x98\x88', 1)

DECLARE @user_create INT
DECLARE @chat_id INT
DECLARE @file_group_id VARCHAR(4096)
DECLARE @count_reader INT
SET @count_reader = 50;

DECLARE @couter INT
SET @couter = 2;

WHILE @couter < @count_reader
BEGIN
INSERT INTO File_group(caption)
		VALUES ('Тестовое сообщение ' + CAST(@couter AS varchar(100)));
		SET @couter = @couter + 1;
END;

GO

DECLARE @user_create INT
DECLARE @chat_id INT
DECLARE @file_group_id VARCHAR(4096)
DECLARE @count_reader INT
SET @count_reader = 49;

DECLARE @couter INT
SET @couter = 1;

WHILE @couter < @count_reader
BEGIN
INSERT INTO File_main(file_link, size, file_group_id)
		VALUES ('https://photo' + CAST(@couter AS varchar(100)) + '.jpg', @couter+10, @couter);
		SET @couter = @couter + 1;
END;

GO
	
DECLARE @user_create INT
DECLARE @chat_id INT
DECLARE @file_group_id VARCHAR(4096)
DECLARE @count_reader INT
SET @count_reader = 49;

DECLARE @couter INT
SET @couter = 1;

WHILE @couter < @count_reader
BEGIN
	
	SET @user_create = @couter;
	SET @chat_id = @couter;
	SET @file_group_id = @couter
	DECLARE @chat_message_id BIGINT;
    INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (DATEADD(DAY, RAND() * DATEDIFF(DAY, '2020-10-12 10:28:51.000', '2020-12-12 10:28:51.000'), (SELECT registration_date FROM User_main WHERE user_id = @user_create)), @user_create, 3, @file_group_id, 1, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	UPDATE File_group
		SET chat_message_id = @@IDENTITY
		WHERE file_group_id = @file_group_id;
	INSERT Private_chat_Chat_messages (chat_message_id, private_chat_id)
		VALUES (@@IDENTITY, @chat_id);
		SET @couter = @couter + 1;
END;

GO


DECLARE @sticker_id INT
DECLARE @caption VARCHAR(255)
DECLARE @couter_sticker INT
DECLARE @chat_id INT

DECLARE @user_create INT

DECLARE @count_reader INT
SET @count_reader = 50;

DECLARE @couter INT
SET @couter = 2;

SET @couter_sticker = 0;

WHILE @couter < @count_reader
BEGIN
SET @sticker_id = FLOOR(1 + (RAND() * (8 - 1 + 1)));
SET @chat_id = @couter;
SET @caption = @couter;

SET @user_create = @couter;
	INSERT INTO Sticker_in_Chat_messages (sticker_id, caption)
		VALUES (@sticker_id, @caption);
	DECLARE @sticker_in_chat_messages_id BIGINT, @chat_message_id BIGINT;
	SET @sticker_in_chat_messages_id = @@IDENTITY;
	INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (DATEADD(DAY, RAND() * DATEDIFF(DAY, '2020-10-12 10:28:51.000', '2020-12-12 10:28:51.000'), (SELECT registration_date FROM User_main WHERE user_id = @user_create)), @user_create, 2, @sticker_in_chat_messages_id, 1, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	UPDATE Sticker_in_Chat_messages
		SET chat_message_id = @@IDENTITY
		WHERE sticker_in_chat_messages_id = @sticker_in_chat_messages_id;
	INSERT Private_chat_Chat_messages (chat_message_id, private_chat_id)
		VALUES (@chat_message_id, @chat_id);
		SET @couter = @couter + 1;
END

GO

DECLARE @sticker_id INT
DECLARE @caption VARCHAR(255)
DECLARE @couter_sticker INT
DECLARE @chat_id INT

DECLARE @user_create INT

DECLARE @count_reader INT
SET @count_reader = 50;

DECLARE @couter INT
SET @couter = 2;

SET @couter_sticker = 0;

WHILE @couter < @count_reader
BEGIN
SET @sticker_id = FLOOR(1 + (RAND() * (8 - 1 + 1)));
SET @chat_id = @couter;
SET @caption = @couter;

SET @user_create = @couter;
	INSERT INTO Sticker_in_Chat_messages (sticker_id, caption)
		VALUES (@sticker_id, @caption);
	DECLARE @sticker_in_chat_messages_id BIGINT, @chat_message_id BIGINT;
	SET @sticker_in_chat_messages_id = @@IDENTITY;
	INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (DATEADD(DAY, RAND() * DATEDIFF(DAY, '2020-10-12 10:28:51.000', '2020-12-12 10:28:51.000'), (SELECT registration_date FROM User_main WHERE user_id = 3)), 3, 2, @sticker_in_chat_messages_id, 1, 3);
	SET @chat_message_id = @@IDENTITY;
	UPDATE Sticker_in_Chat_messages
		SET chat_message_id = @@IDENTITY
		WHERE sticker_in_chat_messages_id = @sticker_in_chat_messages_id;
	INSERT Private_chat_Chat_messages (chat_message_id, private_chat_id)
		VALUES (@chat_message_id, @chat_id);
		SET @couter = @couter + 1;
END

--SELECT * FROM Chat_messages

GO



DECLARE @user_create INT
DECLARE @chat_id INT
DECLARE @text VARCHAR(4096)
DECLARE @count_reader INT
SET @count_reader = 49;

DECLARE @couter INT
SET @couter = 1;

WHILE @couter < @count_reader
BEGIN
	
	
	SET @user_create = @couter;
	SET @chat_id = @couter;
	SET @text = 'Тестовое сообщение ' + CAST(@couter AS varchar(100))

	INSERT INTO Text_message(message_text)
		VALUES (@text);
	DECLARE @text_message_id BIGINT, @chat_message_id BIGINT;
	SET @text_message_id = @@IDENTITY;
	INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES  (DATEADD(DAY, RAND() * DATEDIFF(DAY, '2020-10-12 10:28:51.000', '2020-12-12 10:28:51.000'), (SELECT registration_date FROM User_main WHERE user_id = @user_create)), @user_create, 1, @text_message_id, 1, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	UPDATE Text_message
		SET chat_message_id = @chat_message_id
		WHERE text_message_id = @text_message_id;
	INSERT Private_chat_Chat_messages (chat_message_id, private_chat_id)
		VALUES (@chat_message_id, @chat_id);
		
	SET @couter = @couter + 1; 
	END


	GO
--DELETE FROM Chat_messagesresult TINYINT;

DECLARE @downloading_user_id INT
DECLARE @file_id INT
DECLARE @count_reader INT
SET @count_reader = 48;

DECLARE @couter INT
SET @couter = 2;

WHILE @couter < @count_reader
BEGIN
	
	
	SET @downloading_user_id = @couter;
	SET @file_id = @couter;
	INSERT INTO User_downloaded_file 
	(download_time, downloading_user_id, file_id) 
	VALUES 
	(DATEADD(DAY, RAND() * DATEDIFF(DAY, '2020-11-12 10:28:51.000', '2020-12-12 10:28:51.000'), (SELECT registration_date FROM User_main WHERE user_id = @downloading_user_id)), @downloading_user_id, @file_id)

	SET @couter = @couter + 1; 
	END


	GO
--DELETE FROM Chat_messagesresult TINYINT;


INSERT INTO Block_type 
(name) 
VALUES 
('полная блокировка'), ('запрет написания сообщений'), ('запрет на добавление пользователей')

INSERT INTO Blocked_user 
(block_time_in_days, blocked_user_id, blocking_user_id, block_type_id, block_date) 
VALUES 
(30, 7, 3, 1, GETDATE())

INSERT INTO User_contacts 
(user_recorded_name, blocked, user_id, user_contact_id) 
VALUES 
('Алексей', 0, 1, 2), ('Ваня', 0, 2, 1)

INSERT INTO User_contacts_not_registered_in_app 
(user_contact_phone_number, user_recorded_name) 
VALUES 
('89065838796', 'Брат'), ('89507405217', 'Мама'), ('89104783215','Сестра'), ('89104783215', 'Неизвестный')


INSERT INTO Phonebook_contacts_not_registered_in_app 
(user_id, unregistered_user_contact_id) 
VALUES 
(3, 2), (3, 1)

INSERT INTO Messages_read 
(user_id, chat_message_id) 
VALUES 
(8, 8),
(9, 9)


GO
UPDATE [dbo].[Notifications]
	SET [send_time] = DATEADD(MINUTE, (ABS(CHECKSUM(NEWID())) % 1440), CAST(CAST([send_time] AS DATE) AS DATETIME))
GO