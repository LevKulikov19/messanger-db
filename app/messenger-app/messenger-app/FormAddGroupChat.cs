﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace messenger_app
{
    public partial class FormAddGroupChat : Form
    {
        SqlConnection sqlConnection;
        int userID;
        bool update;
        int updateChatID;
        public FormAddGroupChat(SqlConnection sqlConnection, int userID, bool update, int updateChatID = -1)
        {
            InitializeComponent();
            this.sqlConnection = sqlConnection;
            this.userID = userID;

            if (update)
            {
                button1.Text = "Обновить чат";
                this.Text = "Изменение чата";
            }
            this.update = update;
            this.updateChatID = updateChatID;
        }

        private void FormAddGroupChat_Load(object sender, EventArgs e)
        {

        }

        public void setChatName (string name)
        {
            textBoxName.Text = name;
        }

        public void setUsersPhone(string[] name)
        {
            listBox1.Items.Clear();
            foreach (var item in name)
            {
                listBox1.Items.Add(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text == "")
            {
                MessageBox.Show("Введите название чата");
                return;
            }
            if (update) updateGroupChat();
            else addNewGroupChat();

            Close();
        }

        private void addNewGroupChat()
        {
            int chatID = db_addNewGroupChat(textBoxName.Text);
            db_addMeToGroupChat(chatID);
            addUserFromListToDBGroupChat(chatID);
        }

        private void updateGroupChat()
        {
            db_updateNameGroupChat(updateChatID, textBoxName.Text);
            db_cleanUsersGroupChat(updateChatID);
            db_addMeToGroupChat(updateChatID);
            addUserFromListToDBGroupChat(updateChatID);
        }

        private void addUserFromListToDBGroupChat (int chatID)
        {
            foreach (var item in listBox1.Items)
            {
                if (!db_addUserToGroupChat(item.ToString().Replace("+", ""), chatID))
                {
                    MessageBox.Show($"Пользватель, с номером телефона {item.ToString()}, не может быть добавлен");
                }
            }
        }

        private void db_updateNameGroupChat (int chatID, string name)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query = $"UPDATE Group_chat SET name = '{name}' WHERE group_chat_id = {chatID}";
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void db_cleanUsersGroupChat(int chatID)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query = $"DELETE FROM Group_chat_participants WHERE group_chat_id = {chatID}";
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private int db_addNewGroupChat(string name)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query = $"INSERT INTO Group_chat (name, creator_user_id) VALUES ('{name}', {userID}); SELECT @@IDENTITY;";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                int result = -1;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        result = int.Parse(reader.GetDecimal(0).ToString());
                    }

                }
                reader.Close();
                return result;
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private bool db_addUserToGroupChat(string phone, int chatID)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query = $"DECLARE @return_value int; EXEC @return_value = add_user_to_group_chat '{phone}', {chatID}; SELECT 'Return Value' = @return_value";
                bool result = false;
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        result = reader.GetInt32(0) == 1;
                    }

                }
                reader.Close();
                return result;
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void db_addMeToGroupChat(int chatID)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query = $"INSERT INTO Group_chat_participants (group_chat_id, user_id) VALUES ({chatID}, {userID})";
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show($"Пользватель, с номером телефона {textBox1.Text}, не может быть добавлен");
                return;
            }

            listBox1.Items.Add(textBox1.Text);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1) return;
            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
        }
    }
}
