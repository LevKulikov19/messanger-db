﻿namespace messenger_app
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tab1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonTab1Update = new System.Windows.Forms.Button();
            this.buttonTab1Grep = new System.Windows.Forms.Button();
            this.dataGridViewTab1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelTab2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonTab2Update = new System.Windows.Forms.Button();
            this.chartTab2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelTab3 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelTab3Year = new System.Windows.Forms.Label();
            this.numericUpDownTab3Year = new System.Windows.Forms.NumericUpDown();
            this.buttonTab3Update = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelTab3Month = new System.Windows.Forms.Label();
            this.numericUpDownTab3Month = new System.Windows.Forms.NumericUpDown();
            this.chartTab3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl.SuspendLayout();
            this.tab1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTab1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanelTab2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTab2)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanelTab3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTab3Year)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTab3Month)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTab3)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tab1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(978, 544);
            this.tabControl.TabIndex = 0;
            // 
            // tab1
            // 
            this.tab1.Controls.Add(this.tableLayoutPanel1);
            this.tab1.Location = new System.Drawing.Point(4, 29);
            this.tab1.Name = "tab1";
            this.tab1.Padding = new System.Windows.Forms.Padding(3);
            this.tab1.Size = new System.Drawing.Size(970, 511);
            this.tab1.TabIndex = 0;
            this.tab1.Text = "Сбор статистики";
            this.tab1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.buttonTab1Update, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonTab1Grep, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewTab1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(964, 505);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // buttonTab1Update
            // 
            this.buttonTab1Update.Location = new System.Drawing.Point(3, 468);
            this.buttonTab1Update.Name = "buttonTab1Update";
            this.buttonTab1Update.Size = new System.Drawing.Size(129, 34);
            this.buttonTab1Update.TabIndex = 0;
            this.buttonTab1Update.Text = "Обновить";
            this.buttonTab1Update.UseVisualStyleBackColor = true;
            this.buttonTab1Update.Click += new System.EventHandler(this.buttonTab1Update_Click);
            // 
            // buttonTab1Grep
            // 
            this.buttonTab1Grep.Location = new System.Drawing.Point(485, 468);
            this.buttonTab1Grep.Name = "buttonTab1Grep";
            this.buttonTab1Grep.Size = new System.Drawing.Size(180, 34);
            this.buttonTab1Grep.TabIndex = 1;
            this.buttonTab1Grep.Text = "Собрать статистику";
            this.buttonTab1Grep.UseVisualStyleBackColor = true;
            this.buttonTab1Grep.Click += new System.EventHandler(this.buttonTab1Grep_Click);
            // 
            // dataGridViewTab1
            // 
            this.dataGridViewTab1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel1.SetColumnSpan(this.dataGridViewTab1, 2);
            this.dataGridViewTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTab1.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTab1.Name = "dataGridViewTab1";
            this.dataGridViewTab1.ReadOnly = true;
            this.dataGridViewTab1.RowHeadersWidth = 62;
            this.dataGridViewTab1.RowTemplate.Height = 28;
            this.dataGridViewTab1.Size = new System.Drawing.Size(958, 459);
            this.dataGridViewTab1.TabIndex = 2;
            this.dataGridViewTab1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTab1_CellContentClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanelTab2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(970, 511);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Время отправки уведомлений";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelTab2
            // 
            this.tableLayoutPanelTab2.ColumnCount = 2;
            this.tableLayoutPanelTab2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTab2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTab2.Controls.Add(this.buttonTab2Update, 0, 1);
            this.tableLayoutPanelTab2.Controls.Add(this.chartTab2, 0, 0);
            this.tableLayoutPanelTab2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelTab2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelTab2.Name = "tableLayoutPanelTab2";
            this.tableLayoutPanelTab2.RowCount = 2;
            this.tableLayoutPanelTab2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelTab2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelTab2.Size = new System.Drawing.Size(964, 505);
            this.tableLayoutPanelTab2.TabIndex = 1;
            // 
            // buttonTab2Update
            // 
            this.buttonTab2Update.Location = new System.Drawing.Point(3, 468);
            this.buttonTab2Update.Name = "buttonTab2Update";
            this.buttonTab2Update.Size = new System.Drawing.Size(129, 34);
            this.buttonTab2Update.TabIndex = 0;
            this.buttonTab2Update.Text = "Обновить";
            this.buttonTab2Update.UseVisualStyleBackColor = true;
            this.buttonTab2Update.Click += new System.EventHandler(this.buttonTab2Update_Click);
            // 
            // chartTab2
            // 
            chartArea1.Name = "ChartArea1";
            this.chartTab2.ChartAreas.Add(chartArea1);
            this.tableLayoutPanelTab2.SetColumnSpan(this.chartTab2, 2);
            this.chartTab2.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Enabled = false;
            legend1.Name = "Legend1";
            this.chartTab2.Legends.Add(legend1);
            this.chartTab2.Location = new System.Drawing.Point(3, 3);
            this.chartTab2.Name = "chartTab2";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartTab2.Series.Add(series1);
            this.chartTab2.Size = new System.Drawing.Size(958, 459);
            this.chartTab2.TabIndex = 1;
            this.chartTab2.Text = "chart1";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanelTab3);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(970, 511);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Количество новых пользователей на указанный месяц";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelTab3
            // 
            this.tableLayoutPanelTab3.ColumnCount = 4;
            this.tableLayoutPanelTab3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelTab3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelTab3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelTab3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelTab3.Controls.Add(this.flowLayoutPanel2, 3, 1);
            this.tableLayoutPanelTab3.Controls.Add(this.buttonTab3Update, 0, 1);
            this.tableLayoutPanelTab3.Controls.Add(this.flowLayoutPanel1, 2, 1);
            this.tableLayoutPanelTab3.Controls.Add(this.chartTab3, 0, 0);
            this.tableLayoutPanelTab3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelTab3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelTab3.Name = "tableLayoutPanelTab3";
            this.tableLayoutPanelTab3.RowCount = 2;
            this.tableLayoutPanelTab3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelTab3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelTab3.Size = new System.Drawing.Size(964, 505);
            this.tableLayoutPanelTab3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.labelTab3Year);
            this.flowLayoutPanel2.Controls.Add(this.numericUpDownTab3Year);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(726, 468);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(235, 34);
            this.flowLayoutPanel2.TabIndex = 4;
            // 
            // labelTab3Year
            // 
            this.labelTab3Year.AutoSize = true;
            this.labelTab3Year.Location = new System.Drawing.Point(3, 0);
            this.labelTab3Year.Name = "labelTab3Year";
            this.labelTab3Year.Padding = new System.Windows.Forms.Padding(5);
            this.labelTab3Year.Size = new System.Drawing.Size(48, 30);
            this.labelTab3Year.TabIndex = 1;
            this.labelTab3Year.Text = "Год";
            // 
            // numericUpDownTab3Year
            // 
            this.numericUpDownTab3Year.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownTab3Year.Location = new System.Drawing.Point(57, 3);
            this.numericUpDownTab3Year.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.numericUpDownTab3Year.Name = "numericUpDownTab3Year";
            this.numericUpDownTab3Year.Size = new System.Drawing.Size(165, 26);
            this.numericUpDownTab3Year.TabIndex = 2;
            // 
            // buttonTab3Update
            // 
            this.buttonTab3Update.Location = new System.Drawing.Point(3, 468);
            this.buttonTab3Update.Name = "buttonTab3Update";
            this.buttonTab3Update.Size = new System.Drawing.Size(129, 34);
            this.buttonTab3Update.TabIndex = 0;
            this.buttonTab3Update.Text = "Обновить";
            this.buttonTab3Update.UseVisualStyleBackColor = true;
            this.buttonTab3Update.Click += new System.EventHandler(this.buttonTab3Update_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.labelTab3Month);
            this.flowLayoutPanel1.Controls.Add(this.numericUpDownTab3Month);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(485, 468);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(235, 34);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // labelTab3Month
            // 
            this.labelTab3Month.AutoSize = true;
            this.labelTab3Month.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTab3Month.Location = new System.Drawing.Point(3, 0);
            this.labelTab3Month.Name = "labelTab3Month";
            this.labelTab3Month.Padding = new System.Windows.Forms.Padding(5);
            this.labelTab3Month.Size = new System.Drawing.Size(67, 32);
            this.labelTab3Month.TabIndex = 1;
            this.labelTab3Month.Text = "Месяц";
            this.labelTab3Month.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDownTab3Month
            // 
            this.numericUpDownTab3Month.Location = new System.Drawing.Point(76, 3);
            this.numericUpDownTab3Month.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownTab3Month.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTab3Month.Name = "numericUpDownTab3Month";
            this.numericUpDownTab3Month.Size = new System.Drawing.Size(141, 26);
            this.numericUpDownTab3Month.TabIndex = 2;
            this.numericUpDownTab3Month.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chartTab3
            // 
            chartArea2.Name = "ChartArea1";
            this.chartTab3.ChartAreas.Add(chartArea2);
            this.tableLayoutPanelTab3.SetColumnSpan(this.chartTab3, 4);
            this.chartTab3.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Enabled = false;
            legend2.Name = "Legend1";
            this.chartTab3.Legends.Add(legend2);
            this.chartTab3.Location = new System.Drawing.Point(3, 3);
            this.chartTab3.Name = "chartTab3";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartTab3.Series.Add(series2);
            this.chartTab3.Size = new System.Drawing.Size(958, 459);
            this.chartTab3.TabIndex = 5;
            this.chartTab3.Text = "chart1";
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 544);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormAdmin";
            this.Text = "Администратор";
            this.Load += new System.EventHandler(this.FormAdmin_Load);
            this.tabControl.ResumeLayout(false);
            this.tab1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTab1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanelTab2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartTab2)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanelTab3.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTab3Year)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTab3Month)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTab3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tab1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonTab1Update;
        private System.Windows.Forms.Button buttonTab1Grep;
        private System.Windows.Forms.DataGridView dataGridViewTab1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTab2;
        private System.Windows.Forms.Button buttonTab2Update;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTab3;
        private System.Windows.Forms.Button buttonTab3Update;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTab2;
        private System.Windows.Forms.NumericUpDown numericUpDownTab3Month;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label labelTab3Month;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label labelTab3Year;
        private System.Windows.Forms.NumericUpDown numericUpDownTab3Year;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTab3;
    }
}