﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Rebar;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Window;

namespace messenger_app
{
    public partial class FormUserChat : Form
    {
        int chatID;
        int userID;
        bool is_private;
        SqlConnection sqlConnection = null;
        public FormUserChat(SqlConnection sqlConnection, bool is_private, int chatID, int userID)
        {
            InitializeComponent();
            this.chatID = chatID;
            this.sqlConnection = sqlConnection;
            this.is_private = is_private;
            this.userID = userID;
        }

        private void FormUserChat_Load(object sender, EventArgs e)
        {
            initChatTable();
            refrashChatTable();
            refreshStateUpdateTextMessageButton();
        }
        
        private void initChatTable ()
        {
            dataGridViewChat.Columns.Add("chat_message_id", "ID");
            dataGridViewChat.Columns.Add("time", "Дата");
            dataGridViewChat.Columns.Add("nickname", "Полльзователь, отправитель");
            dataGridViewChat.Columns.Add("message_type", "Тип сообщения");
            dataGridViewChat.Columns.Add("text", "Текст");
            dataGridViewChat.Columns.Add("options", "Дополнительно");
        }
        private void readSingleRowChatTable(DataGridView dgv, IDataRecord record)
        {
            if (record == null) return;

            dgv.Rows.Add(
                record.GetInt64(0),
                record.GetDateTime(1),
                record.GetString(2),
                record.GetString(3),
                record.GetString(4),
                record.GetString(5));
        }

        private void refrashChatTable()
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                dataGridViewChat.Rows.Clear();
                string is_private_q;
                if (is_private)
                {
                    is_private_q = "1";
                }   
                else
                {
                    is_private_q = "0";
                }
                string query = $"get_message_chat {is_private_q}, {chatID}";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        readSingleRowChatTable(dataGridViewChat, reader);
                    }

                }
                reader.Close();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            refrashChatTable();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonText_Click(object sender, EventArgs e)
        {
            string result = null;
            using (var form = new FormInputTextMessage(false))
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    result = form.InputText;
                }
            }
            if (result == null) return;
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query;
                if (is_private)
                    query = $"add_message_text_in_private_chat {userID}, {chatID}, '{result}'";
                else
                    query = $"add_message_text_in_group_chat {userID}, {chatID}, '{result}'";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
            refrashChatTable();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewChat.CurrentRow.Cells[0].Value == null)
            {
                MessageBox.Show("Выбирите не пустую ячейку");
                return;
            }
            int messageID = int.Parse(dataGridViewChat.CurrentRow.Cells[0].Value.ToString());
            if (db_deleteChatMessage(messageID))
            {
                refrashChatTable();
            }
            else
            {
                MessageBox.Show("Не удалось удалить сообщение");
            }
        }

        private bool db_deleteChatMessage(int messageID)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query = $"DECLARE @return_value int; EXEC @return_value = delete_message {userID}, {messageID}; SELECT 'Return Value' = @return_value";
                bool result = false;
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        result = reader.GetInt32(0) == 1;
                    }

                }
                reader.Close();
                return result;
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void buttonUpdateText_Click(object sender, EventArgs e)
        {
            int messageID = int.Parse(dataGridViewChat.CurrentRow.Cells[0].Value.ToString());
            string textOld = dataGridViewChat.CurrentRow.Cells[4].Value.ToString();
            string text = null;
            using (var form = new FormInputTextMessage(true, textOld))
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    text = form.InputText;
                }
            }
            if (text != null)
            {
                if (db_updateTextChatMessage(messageID, text))
                {
                    refrashChatTable();
                }
                else
                {
                    MessageBox.Show("Не удалось изменить сообщение");
                }
            }

        }

        private bool db_updateTextChatMessage(int messageID, string text)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query = $"DECLARE @return_value int; EXEC @return_value = update_text_message {userID}, {messageID}, '{text}'; SELECT 'Return Value' = @return_value";
                bool result = false;
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        result = reader.GetInt32(0) == 1;
                    }

                }
                reader.Close();
                return result;
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void dataGridViewChat_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            refreshStateUpdateTextMessageButton();
        }

        private void refreshStateUpdateTextMessageButton ()
        {
            if (dataGridViewChat == null) return;
            if (dataGridViewChat.CurrentRow == null) return;
            if (dataGridViewChat.CurrentRow.Cells[3] == null) return;
            if (dataGridViewChat.CurrentRow.Cells[3].Value == null ) return;
            if (dataGridViewChat.CurrentRow.Cells[3].Value.ToString() == "Текстовое сообщение")
            {
                buttonUpdateText.Enabled = true;
            }
            else
            {
                buttonUpdateText.Enabled = false;
            }
        }
    }
}
