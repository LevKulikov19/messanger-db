﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace messenger_app
{
    public partial class FormUser : Form
    {
        SqlConnection sqlConnection = null;
        int userID;
        public FormUser(SqlConnection sqlConnection, int userID)
        {
            InitializeComponent();
            this.sqlConnection = sqlConnection;
            this.userID = userID;

            initGroupChatTable();
            initPrivateChatTable();
            refreshGroupChatTable();
            refreshPrivateChatTable();
            actionButtonEnabled(false);
        }

        private void FormUser_Load(object sender, EventArgs e)
        {

        }

        private void buttonOpenChat_Click(object sender, EventArgs e)
        {
            if (dataGridViewGroupChat.CurrentRow.Cells[0].Value == null)
            {
                MessageBox.Show("Выбирите не пустую ячейку");
                return;
            }
            bool is_pivate_chat = tabControl1.SelectedIndex == 1;
            int chatID_select = -1;
            if (is_pivate_chat)
            {

                chatID_select = int.Parse(dataGridViewPrivateChat.CurrentRow.Cells[0].Value.ToString());
            }
            else
            {
                chatID_select = int.Parse(dataGridViewGroupChat.CurrentRow.Cells[0].Value.ToString());
            }
            FormUserChat userChat = new FormUserChat(sqlConnection, is_pivate_chat, chatID_select, userID);
            userChat.ShowDialog();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            int chatID_select = int.Parse(dataGridViewGroupChat.CurrentRow.Cells[0].Value.ToString());
            db_deleteGroupChat(chatID_select);
            refreshGroupChatTable();

        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            FormAddGroupChat addGroupChat = new FormAddGroupChat(sqlConnection, userID, false);
            addGroupChat.ShowDialog();
            refreshGroupChatTable();
        }

        private void buttonUpdateList_Click(object sender, EventArgs e)
        {
            refreshGroupChatTable();
            refreshPrivateChatTable();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            int chatID_select = int.Parse(dataGridViewGroupChat.CurrentRow.Cells[0].Value.ToString());
            string chatName = dataGridViewGroupChat.CurrentRow.Cells[1].Value.ToString();
            FormAddGroupChat addGroupChat = new FormAddGroupChat(sqlConnection, userID, true, chatID_select);
            addGroupChat.setChatName(chatName);
            addGroupChat.setUsersPhone(getUserPhoneGroupChat(chatID_select));
            addGroupChat.ShowDialog();
            refreshGroupChatTable();
        }

        private void initGroupChatTable()
        {
            dataGridViewGroupChat.Columns.Add("group_chat_id", "ID");
            dataGridViewGroupChat.Columns.Add("name", "Название чата");
            dataGridViewGroupChat.Columns.Add("creator_user_id", "ID создателя чата");
        }

        private void ReadSingleRowGroupChatTable(DataGridView dgv, IDataRecord record)
        {
            if (record == null) return;

            int createID = record.GetInt32(2);

            int number = dgv.Rows.Add(record.GetInt32(0), record.GetString(1), createID);
            if (createID == userID)
            {
                dgv[0, number].Style.BackColor = Color.LightYellow;
                dgv[1, number].Style.BackColor = Color.LightYellow;
                dgv[2, number].Style.BackColor = Color.LightYellow;
            }
        }

        private void refreshGroupChatTable()
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                dataGridViewGroupChat.Rows.Clear();
                string query = 
                $"SELECT active_group_chat.group_chat_id, name, creator_user_id  FROM Group_chat_participants JOIN active_group_chat ON Group_chat_participants.group_chat_id = active_group_chat.group_chat_id WHERE user_id = {userID}";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        ReadSingleRowGroupChatTable(dataGridViewGroupChat, reader);
                    }

                }
                reader.Close();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private string[] getUserPhoneGroupChat(int chatID)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                List<string> result = new List<string> ();
                dataGridViewGroupChat.Rows.Clear();
                string query =
                $"SELECT phone_number FROM Group_chat_participants JOIN all_base_user ON Group_chat_participants.user_id = all_base_user.user_id WHERE Group_chat_participants.group_chat_id = {chatID} AND all_base_user.user_id <> {userID}";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        result.Add (reader.GetString(0));
                    }

                }
                reader.Close();
                return result.ToArray();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void initPrivateChatTable()
        {
            dataGridViewPrivateChat.Columns.Add("private_chat_id", "ID");
            dataGridViewPrivateChat.Columns.Add("user_id", "ID собеседника");
            dataGridViewPrivateChat.Columns.Add("first_name", "Имя собеседника");
            dataGridViewPrivateChat.Columns.Add("last_name", "Фамилия собеседника");
            dataGridViewPrivateChat.Columns.Add("middle_name", "Отчество собеседника");
        }

        private void ReadSingleRowPrivateChatTable(DataGridView dgv, IDataRecord record)
        {
            if (record == null) return;

            dgv.Rows.Add(
                record.GetInt32(0),
                record.GetInt32(1),
                record.GetString(2),
                record.GetString(3),
                record.GetString(4));
        }

        private void refreshPrivateChatTable()
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                dataGridViewPrivateChat.Rows.Clear();
                string query =
                $"SELECT private_chat_id, user_id, first_name, last_name, middle_name FROM (SELECT private_chat_id, CASE WHEN user_1_id = 1 THEN user_2_id ELSE user_1_id END AS user_to FROM Private_chat WHERE user_1_id = {userID} OR user_2_id = {userID}) a JOIN all_base_user ON all_base_user.user_id = a.user_to";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        ReadSingleRowPrivateChatTable(dataGridViewPrivateChat, reader);
                    }

                }
                reader.Close();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void dataGridViewPrivateChat_CellClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void actionButtonEnabled(bool enable)
        {
            buttonDeleteChat.Enabled = enable;
            buttonUpdateChat.Enabled = enable;
        }

        private void dataGridViewGroupChat_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridViewGroupChat[2, e.RowIndex].Value == null) return;
                actionButtonEnabled(
                    int.Parse(dataGridViewGroupChat[2, e.RowIndex].Value.ToString()) == userID
                );
            }
            catch (Exception)
            {
                dataGridViewGroupChat.ClearSelection();
            }
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            dataGridViewGroupChat.ClearSelection();
            dataGridViewPrivateChat.ClearSelection();
            actionButtonEnabled(false);
        }

        private void db_deleteGroupChat (int chatID)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query = $"delete_group_chat {userID}, {chatID}";
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }
    }
}
