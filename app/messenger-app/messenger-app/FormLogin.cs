﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace messenger_app
{
    public partial class FormLogin : Form
    {
        const string dbServerName = "ANDREW",
                     dbName = "messanger",
                     dbLoginUser = "Login_user",
                     dbPassLoginUser = "login",
                     dbUser = "User_base",
                     dbPassUser = "user",
                     dbAdmin = "Administarator",
                     dbPassAdmin = "admin";

        string phone;
        enum Role {
            Login = 0,
            Admin = 1,
            User = 2
        }

        Role roleState = Role.Login;
        int userID = -1;

        SqlConnection sqlConnectionLogin = null;
        
        public FormLogin()
        {
            InitializeComponent();
            labelLoginError.Visible = false;

            string connectionLoginData = $"Data Source={dbServerName};" +
                                         $"Initial Catalog={dbName};" +
                                         $"User Id={dbLoginUser};" +
                                         $"Password={dbPassLoginUser};" +
                                         "Integrated Security=false;" +
                                         "Connect Timeout=3";

            sqlConnectionLogin = new SqlConnection(connectionLoginData);
            sqlConnectionLogin.Open();
            textBoxPhone.Text = "70000000000";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonAction_Click(object sender, EventArgs e)
        {
            buttonLogin.Enabled = false;
            string code = "-1";
            labelLoginError.Visible = false;
            phone = ProcessingPhoneNumber(textBoxPhone.Text);
            code = db_generate_code_verify(phone);
            if (code != "-1")
            {
                FormInputVerifyCode formCode = new FormInputVerifyCode(code);
                formCode.SendCode += FormCode_SendCode;
                formCode.ShowDialog();
            }
            else
            {
                labelLoginError.Visible = true;
            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            string connection = null;
            SqlConnection sqlConnectionRole = null;
            if (roleState == Role.User)
            {
                connection = $"Data Source={dbServerName};" +
                                         $"Initial Catalog={dbName};" +
                                         $"User Id={dbUser};" +
                                         $"Password={dbPassUser};" +
                                         "Integrated Security=false;" +
                                         "Connect Timeout=3";
            }
            else if (roleState == Role.Admin)
            {
                connection = $"Data Source={dbServerName};" +
                         $"Initial Catalog={dbName};" +
                         $"User Id={dbAdmin};" +
                         $"Password={dbPassAdmin};" +
                         "Integrated Security=false;" +
                         "Connect Timeout=3";
            }
            else
            {
                throw new Exception("Error: user not support");
            }
            sqlConnectionRole = new SqlConnection(connection);
            sqlConnectionRole.Open();
            Form form = null;
            if (roleState == Role.User)
                form = new FormUser(sqlConnectionRole, userID);
            else if (roleState == Role.Admin)
                form = new FormAdmin(sqlConnectionRole, userID);
            form.ShowDialog();
            db_logout(phone);
            sqlConnectionRole.Close();

        }

        private void FormCode_SendCode(string code)
        {
            if (db_code_verify(phone, code))
                buttonLogin.Enabled = true;
            else
                labelLoginError.Visible = true;
        }

        private void db_logout(string phone)
        {
            if (sqlConnectionLogin != null && sqlConnectionLogin.State == ConnectionState.Open)
            {
                string query = $"logout {phone}";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnectionLogin);
                sqlCommand.ExecuteNonQuery();
                resetState();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private bool db_code_verify(string phone, string code)
        {
            if (sqlConnectionLogin != null && sqlConnectionLogin.State == ConnectionState.Open)
            {
                int role = -1;
                string query = $"validate_verify_code '{phone}', {code}";
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnectionLogin);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    reader.Read();
                    role = reader.GetInt32(0);
                    if (role != -1)
                    {
                        userID = reader.GetInt32(1);
                        if (role == 1) roleState = Role.User;
                        else if (role == 4) roleState = Role.Admin;
                    }

                }
                reader.Close();
                return role != -1;
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }
        private string db_generate_code_verify(string phone)
        {
            if (sqlConnectionLogin != null && sqlConnectionLogin.State == ConnectionState.Open)
            {
                string code = null;
                string query = "get_verify_code '" + phone + "'";
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnectionLogin);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    reader.Read();
                    code = reader.GetValue(0).ToString();
                }
                reader.Close();
                return code;
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }
        public string ProcessingPhoneNumber(string phoneNumber)
        {
            return phoneNumber.Replace("+", "");
        }

        private void resetState()
        {
            roleState = Role.Login;
            userID = -1;
            phone = null;
            buttonLogin.Enabled = false;
        }
    }
}
