﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace messenger_app
{
    public partial class FormAdmin : Form
    {
        SqlConnection sqlConnection = null;
        int userID;

        public FormAdmin(SqlConnection sqlConnection, int userID)
        {
            InitializeComponent();
            this.sqlConnection = sqlConnection;
            this.userID = userID;
        }

        private void FormAdmin_Load(object sender, EventArgs e)
        {
            initGrepStat();
            refreshGrepStat();

            refreshTimeSendNotification();

            setNumeraticUpDownTab3Default();
            refreshNewUsersSpecifiedMonth();
        }

        private void buttonTab1Update_Click(object sender, EventArgs e)
        {
            refreshGrepStat();
        }

        private void buttonTab1Grep_Click(object sender, EventArgs e)
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                string query = $"add_users_statistics_for_app {userID}";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                MessageBox.Show("Операция выполнена");
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void initGrepStat()
        {
            dataGridViewTab1.Columns.Add("statistics_collection_id", "ID");
            dataGridViewTab1.Columns.Add("number_of_sent_messages", "Количество отправленных сообщений");
            dataGridViewTab1.Columns.Add("number_of_sent_files", "Количество отправленных файлов");
            dataGridViewTab1.Columns.Add("total_volume_of_sent_files", "Объем отправленных файлов, ГБ");
            dataGridViewTab1.Columns.Add("number_of_downloads_of_sent_files", "Количество скачиваний файлов");
            dataGridViewTab1.Columns.Add("number_of_sent_stickers", "Количество отправленных стикиров");
            dataGridViewTab1.Columns.Add("user_id", "ID пользователя, которй собрал статистику");
            dataGridViewTab1.Columns.Add("date_collected", "Дата сбора");
        }

        private void ReadSingleRowGrepStat (DataGridView dgv,IDataRecord record)
        {
            if (record == null) return;

            dgv.Rows.Add(
                record.GetInt32(0),
                record.GetInt32(1),
                record.GetInt32(2),
                record.GetDouble(3),
                record.GetInt32(4),
                record.GetInt32(5),
                record.GetInt32(6),
                record.GetDateTime(7)
            );
        }

        private void refreshGrepStat ()
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                dataGridViewTab1.Rows.Clear();
                StringBuilder query_build = new StringBuilder();
                query_build.Append("SELECT TOP (1000) ");
                query_build.Append("statistics_collection_id, ");
                query_build.Append("number_of_sent_messages, ");
                query_build.Append("number_of_sent_files, ");
                query_build.Append("total_volume_of_sent_files, ");
                query_build.Append("number_of_downloads_of_sent_files, ");
                query_build.Append("number_of_sent_stickers, ");
                query_build.Append("user_id, ");
                query_build.Append("date_collected ");
                query_build.Append("FROM Users_statistics_for_app");
                string query = query_build.ToString();

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while(reader.Read())
                    {
                        ReadSingleRowGrepStat(dataGridViewTab1, reader);
                    }
                    
                }
                reader.Close();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void dataGridViewTab1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ReadSingleSeriesTimeSendNotification(Chart chart, IDataRecord record)
        {
            if (record == null) return;

            chart.Series[0].Points.AddXY(record.GetInt32(0), record.GetInt32(1));
        }

        private void refreshTimeSendNotification()
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                chartTab2.Series[0].Points.Clear();
                string query = "time_sending_notifications_users_by_hour";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        ReadSingleSeriesTimeSendNotification(chartTab2, reader);
                    }

                }
                reader.Close();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }

        private void buttonTab2Update_Click(object sender, EventArgs e)
        {
            refreshTimeSendNotification();
        }

        private void buttonTab3Update_Click(object sender, EventArgs e)
        {
            refreshNewUsersSpecifiedMonth();
        }

        private void setNumeraticUpDownTab3Default()
        {
            DateTime thisDay = DateTime.Today;
            numericUpDownTab3Year.Value = thisDay.Year;
            numericUpDownTab3Month.Value = thisDay.Month;

        }

        private void ReadSingleSeriesNewUsersSpecifiedMonth(Chart chart, IDataRecord record)
        {
            if (record == null) return;

            chart.Series[0].Points.AddXY(record.GetInt32(0), record.GetInt32(1));
        }

        private void refreshNewUsersSpecifiedMonth()
        {
            if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
            {
                chartTab3.Series[0].Points.Clear();
                string query = $"count_new_users_specified_month_by_day {numericUpDownTab3Year.Value}, {numericUpDownTab3Month.Value}";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        ReadSingleSeriesNewUsersSpecifiedMonth(chartTab3, reader);
                    }

                }
                reader.Close();
            }
            else
            {
                throw new Exception("Error: no connection to database");
            }
        }
    }
}
