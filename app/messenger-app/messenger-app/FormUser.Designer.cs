﻿namespace messenger_app
{
    partial class FormUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonUpdateList = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewGroupChat = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridViewPrivateChat = new System.Windows.Forms.DataGridView();
            this.buttonUpdateChat = new System.Windows.Forms.Button();
            this.buttonCreateChat = new System.Windows.Forms.Button();
            this.buttonOpenChat = new System.Windows.Forms.Button();
            this.buttonDeleteChat = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroupChat)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrivateChat)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.buttonUpdateList, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonUpdateChat, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonDeleteChat, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonCreateChat, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonOpenChat, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(994, 450);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // buttonUpdateList
            // 
            this.buttonUpdateList.Location = new System.Drawing.Point(3, 413);
            this.buttonUpdateList.Name = "buttonUpdateList";
            this.buttonUpdateList.Size = new System.Drawing.Size(192, 34);
            this.buttonUpdateList.TabIndex = 0;
            this.buttonUpdateList.Text = "Обновить";
            this.buttonUpdateList.UseVisualStyleBackColor = true;
            this.buttonUpdateList.Click += new System.EventHandler(this.buttonUpdateList_Click);
            // 
            // tabControl1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tabControl1, 5);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(988, 404);
            this.tabControl1.TabIndex = 5;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewGroupChat);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(980, 371);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Групповые чаты";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewGroupChat
            // 
            this.dataGridViewGroupChat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGroupChat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewGroupChat.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewGroupChat.MultiSelect = false;
            this.dataGridViewGroupChat.Name = "dataGridViewGroupChat";
            this.dataGridViewGroupChat.ReadOnly = true;
            this.dataGridViewGroupChat.RowHeadersWidth = 62;
            this.dataGridViewGroupChat.RowTemplate.Height = 28;
            this.dataGridViewGroupChat.Size = new System.Drawing.Size(974, 365);
            this.dataGridViewGroupChat.TabIndex = 3;
            this.dataGridViewGroupChat.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewGroupChat_CellClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridViewPrivateChat);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(980, 371);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Личные чаты";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPrivateChat
            // 
            this.dataGridViewPrivateChat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPrivateChat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPrivateChat.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewPrivateChat.MultiSelect = false;
            this.dataGridViewPrivateChat.Name = "dataGridViewPrivateChat";
            this.dataGridViewPrivateChat.ReadOnly = true;
            this.dataGridViewPrivateChat.RowHeadersWidth = 62;
            this.dataGridViewPrivateChat.RowTemplate.Height = 28;
            this.dataGridViewPrivateChat.Size = new System.Drawing.Size(974, 365);
            this.dataGridViewPrivateChat.TabIndex = 4;
            this.dataGridViewPrivateChat.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPrivateChat_CellClick);
            // 
            // buttonUpdateChat
            // 
            this.buttonUpdateChat.Location = new System.Drawing.Point(795, 413);
            this.buttonUpdateChat.Name = "buttonUpdateChat";
            this.buttonUpdateChat.Size = new System.Drawing.Size(196, 34);
            this.buttonUpdateChat.TabIndex = 6;
            this.buttonUpdateChat.Text = "Изменить чат группы";
            this.buttonUpdateChat.UseVisualStyleBackColor = true;
            this.buttonUpdateChat.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonCreateChat
            // 
            this.buttonCreateChat.Location = new System.Drawing.Point(399, 413);
            this.buttonCreateChat.Name = "buttonCreateChat";
            this.buttonCreateChat.Size = new System.Drawing.Size(192, 34);
            this.buttonCreateChat.TabIndex = 4;
            this.buttonCreateChat.Text = "Создать чат группы";
            this.buttonCreateChat.UseVisualStyleBackColor = true;
            this.buttonCreateChat.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // buttonOpenChat
            // 
            this.buttonOpenChat.Location = new System.Drawing.Point(201, 413);
            this.buttonOpenChat.Name = "buttonOpenChat";
            this.buttonOpenChat.Size = new System.Drawing.Size(192, 34);
            this.buttonOpenChat.TabIndex = 1;
            this.buttonOpenChat.Text = "Открыть чат";
            this.buttonOpenChat.UseVisualStyleBackColor = true;
            this.buttonOpenChat.Click += new System.EventHandler(this.buttonOpenChat_Click);
            // 
            // buttonDeleteChat
            // 
            this.buttonDeleteChat.Location = new System.Drawing.Point(597, 413);
            this.buttonDeleteChat.Name = "buttonDeleteChat";
            this.buttonDeleteChat.Size = new System.Drawing.Size(192, 34);
            this.buttonDeleteChat.TabIndex = 2;
            this.buttonDeleteChat.Text = "Удалить чат группы";
            this.buttonDeleteChat.UseVisualStyleBackColor = true;
            this.buttonDeleteChat.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // FormUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FormUser";
            this.Text = "Пользователь";
            this.Load += new System.EventHandler(this.FormUser_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroupChat)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrivateChat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonUpdateList;
        private System.Windows.Forms.Button buttonOpenChat;
        private System.Windows.Forms.Button buttonDeleteChat;
        private System.Windows.Forms.DataGridView dataGridViewGroupChat;
        private System.Windows.Forms.Button buttonCreateChat;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridViewPrivateChat;
        private System.Windows.Forms.Button buttonUpdateChat;
    }
}