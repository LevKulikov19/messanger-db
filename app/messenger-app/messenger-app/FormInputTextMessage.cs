﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace messenger_app
{
    public partial class FormInputTextMessage : Form
    {

        public FormInputTextMessage(bool update, string oldText = "")
        {
            InitializeComponent();
            if (update)
            {
                this.Text = "Изменить текстовое сообщение";
                button1.Text = "Обновить";
                textBox1.Text = oldText;
            }
        }


        public string InputText
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }

        private void okButton_Click(object sender, EventArgs e)
        {

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {

        }
        

        private void FormInputTextMessage_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void FormInputTextMessage_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}
