-- представление всех текстовых ресурсов
CREATE VIEW all_text_resource (type, id, date, text)
AS
WITH chat AS (
	SELECT *, CASE WHEN edit_time IS NULL THEN send_time ELSE edit_time END AS time FROM Chat_messages WHERE is_moderated = 0
)
SELECT message_type_id, message_id, c.time, message_text FROM Text_message LEFT JOIN chat AS c ON Text_message.chat_message_id = c.chat_message_id
UNION ALL
SELECT message_type_id, message_id, c.time, caption FROM Sticker_in_Chat_messages LEFT JOIN chat AS c ON Sticker_in_Chat_messages.chat_message_id = c.chat_message_id
UNION ALL
SELECT message_type_id, message_id, c.time, caption FROM File_group LEFT JOIN chat AS c ON File_group.chat_message_id = c.chat_message_id
GO

-- представление всех пользователей
CREATE VIEW all_base_user (user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id)
AS
SELECT user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id FROM User_main WHERE role_id = 1
WITH CHECK OPTION
GO

-- представление всех модераторов
CREATE VIEW all_moderator (user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id)
AS
SELECT user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id FROM User_main WHERE role_id = 2
WITH CHECK OPTION
GO

-- представление всех создателей стикиров
CREATE VIEW all_sticker_creator (user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id)
AS
SELECT user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id FROM User_main WHERE role_id = 3
WITH CHECK OPTION
GO

-- представление всех администраторов
CREATE VIEW all_administrator (user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id)
AS
SELECT user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id FROM User_main WHERE role_id = 4
WITH CHECK OPTION
GO

-- представление пользователей, которые находтся в блокировке
CREATE VIEW all_blocked_user (user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id)
AS
SELECT user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id FROM User_main JOIN Blocked_user ON User_main.user_id = Blocked_user.blocked_user_id
GO

-- представление пользователей, которые не получали уведомлений
CREATE VIEW all_dont_have_notification (user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id)
AS
SELECT User_main.user_id, last_name, first_name, middle_name, photo, nickname, online_status, phone_number, registration_date, description, user_settings_id, role_id FROM User_main LEFT JOIN Notifications ON User_main.user_id = Notifications.user_id WHERE Notifications.notification_id IS NULL
GO

CREATE VIEW active_group_chat (group_chat_id, pin_message, name, creator_user_id)
AS
	SELECT group_chat_id, pin_message, name, creator_user_id FROM Group_chat WHERE is_delete = 0
GO
