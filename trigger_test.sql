-- ����� �������� ������ ���� �������� ������ �� ����� init_data.sql

-- �������� �� �� ��� ���� �������� ��������� �� ������ ������� �������������
INSERT INTO Chat_messages
(send_time, edit_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
VALUES
('2022-05-11 12:00:00', '2021-05-11 12:11:00', 1, 1, 1, 1, 1)

DELETE FROM Chat_messages WHERE chat_message_id = @@IDENTITY

-- �������� ��� ���������� ��������� ���� �� ��������� ��������� � ����
INSERT INTO Chat_messages
(send_time, edit_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
VALUES
('2022-05-11 12:00:00', '2022-05-11 12:11:00', 3, 1, 1, 1, 1)

DELETE FROM Chat_messages WHERE chat_message_id = @@IDENTITY

-- �������� � ��������� �������� ����� �� ������
INSERT INTO Chat_messages
(send_time, edit_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
VALUES
('2022-05-11 12:00:00', '2022-05-11 12:11:00', 2, 1, 5, 1, 1)

DELETE FROM Chat_messages WHERE chat_message_id = @@IDENTITY

-- �������� � ��������� �������� ����� �� ���
INSERT INTO Chat_messages
(send_time, edit_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
VALUES
('2022-05-11 12:00:00', '2022-05-11 12:11:00', 1, 1, 1, 1, 5)

DELETE FROM Chat_messages WHERE chat_message_id = @@IDENTITY

-- ������������ �� ����� ��������� ��������� ���� �� �� ������� � ���� ���� ��� ������
INSERT INTO Messages_read
(user_id, chat_message_id)
VALUES
(2, 1)

PRINT(@@IDENTITY)

-- ���� � ����� ������������ � �������: � ������������ ����� ���� �� ����� 1 000 ������� � ����
DECLARE @count_reader INT
SET @count_reader = 1100;
DECLARE @couter INT
SET @couter = 0;
WHILE @couter < @count_reader
BEGIN
INSERT INTO User_account_access
(date_time, operation, user_id)
VALUES
('2019-01-01 12:00:00', '����', 1), ('2019-01-01 12:00:00', '�����', 1)
END

-- ���� � ����� ������������ � �������: �� ����� ���� �������� "���� � �����" ������ �������� � ���� ����� �����������
INSERT INTO User_account_access
(date_time, operation, user_id)
VALUES
('2010-01-01 12:00:00', '����', 1), ('2010-01-01 12:00:00', '�����', 1)

-- ������ ���: user_1_id � user_2_id �� �����
INSERT INTO Private_chat
(user_1_id, user_2_id)
VALUES
(1, 1)

-- ������ ���: ������������ ��������� ������ ��������� � �������� ����
INSERT INTO Private_chat
(pin_message, user_1_id, user_2_id)
VALUES
(1, 1, 2)