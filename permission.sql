-- Обычный пользователь
CREATE LOGIN User_base WITH PASSWORD = 'user';
CREATE USER User_base FOR LOGIN User_base;

GRANT SELECT ON App_theme TO User_base;
GRANT SELECT ON Interface_language TO User_base;
GRANT SELECT ON User_settings TO User_base;
GRANT SELECT ON Notification_sound_settings TO User_base;

GRANT SELECT, INSERT, UPDATE ON all_base_user TO User_base;
GRANT EXECUTE ON get_message_chat TO User_base;

GRANT SELECT, INSERT, UPDATE ON Private_chat TO User_base;
GRANT SELECT, INSERT, UPDATE ON active_group_chat TO User_base;
GRANT SELECT, INSERT, UPDATE, DELETE ON Group_chat_participants TO User_base;
GRANT EXECUTE ON delete_group_chat TO User_base;

GRANT SELECT ON Message_type TO User_base
GRANT SELECT ON Chat_messages TO User_base

GRANT SELECT ON Sticker TO User_base;
GRANT SELECT ON Text_message TO User_base;
GRANT SELECT ON File_group TO User_base;
GRANT SELECT ON File_main TO User_base;

GRANT EXECUTE ON add_message_text_in_private_chat TO User_base;
GRANT EXECUTE ON add_message_text_in_group_chat TO User_base;
GRANT EXECUTE ON add_message_sticker_in_private_chat TO User_base;
GRANT EXECUTE ON add_message_sticker_in_group_chat TO User_base;
GRANT EXECUTE ON add_message_forward_in_private_chat TO User_base;
GRANT EXECUTE ON add_message_forward_in_group_chat TO User_base;
GRANT EXECUTE ON add_message_file_group_in_private_chat TO User_base;
GRANT EXECUTE ON add_message_file_group_in_group_chat TO User_base;

GRANT INSERT ON User_downloaded_file TO User_base;

GRANT SELECT, INSERT, UPDATE, DELETE ON User_contacts_not_registered_in_app TO User_base;
GRANT SELECT, INSERT, UPDATE, DELETE ON Phonebook_contacts_not_registered_in_app TO User_base;
GRANT SELECT, INSERT, UPDATE, DELETE ON User_app_name_from_phonebook TO User_base;
GRANT SELECT, INSERT, UPDATE, DELETE ON User_contacts TO User_base;
GRANT SELECT, INSERT ON User_account_access TO User_base;

GRANT SELECT ON Blocked_user TO User_base;
GRANT SELECT ON Block_type TO User_base;

GRANT EXECUTE ON count_messages_written_per_month_by_user TO User_base;
GRANT EXECUTE ON count_files_sent_and_total_volume_for_month TO User_base;
GRANT EXECUTE ON count_downloads_user_uploaded_files_per_month TO User_base;
GRANT EXECUTE ON count_stickers_sent_per_month TO User_base;
GRANT EXECUTE ON add_user_to_group_chat TO User_base;
GRANT EXECUTE ON delete_message TO User_base; -- NEW
GRANT EXECUTE ON update_text_message TO User_base; -- NEW

GRANT SELECT ON Notifications TO User_base;

-- Создатель стикиров
CREATE LOGIN Creator_sticker WITH PASSWORD = 'sticker';
CREATE USER Creator_sticker FOR LOGIN Creator_sticker;

GRANT EXECUTE ON count_sticker_uses_by_month TO Creator_sticker;

GRANT SELECT ON all_sticker_creator TO Creator_sticker;
GRANT SELECT ON App_theme TO Creator_sticker;
GRANT SELECT ON Interface_language TO Creator_sticker;
GRANT SELECT ON User_settings TO Creator_sticker;
GRANT SELECT ON Notification_sound_settings TO Creator_sticker;

GRANT SELECT ON Notifications TO Creator_sticker;

GRANT SELECT, INSERT, UPDATE, DELETE ON Sticker TO Creator_sticker;


-- Модератор
CREATE LOGIN Moderator WITH PASSWORD = 'moderator';
CREATE USER Moderator FOR LOGIN Moderator;

GRANT SELECT ON all_moderator TO Moderator;
GRANT SELECT ON App_theme TO Moderator;
GRANT SELECT ON Interface_language TO Moderator;
GRANT SELECT ON User_settings TO Moderator;
GRANT SELECT ON Notification_sound_settings TO Moderator;
GRANT SELECT ON Notifications TO Moderator;

GRANT SELECT ON all_text_resource TO Moderator;
GRANT EXECUTE ON ban_user_by_message TO Moderator;

-- Администратор
CREATE LOGIN Administarator WITH PASSWORD = 'admin';
CREATE USER Administarator FOR LOGIN Administarator;

GRANT SELECT, INSERT ON all_administrator TO Administarator;
GRANT SELECT ON App_theme TO Administarator;
GRANT SELECT ON Interface_language TO Administarator;
GRANT SELECT ON User_settings TO Administarator;
GRANT SELECT ON Notification_sound_settings TO Administarator;
GRANT SELECT, INSERT ON Notifications TO Administarator;

GRANT SELECT, INSERT, UPDATE, DELETE ON Sticker TO Creator_sticker;

-- представления
GRANT SELECT ON all_moderator TO Administarator;
GRANT SELECT ON all_sticker_creator TO Administarator;
GRANT SELECT ON all_administrator TO Administarator;
GRANT SELECT ON all_blocked_user TO Administarator;
GRANT SELECT ON all_dont_have_notification TO Administarator;

-- полный доступ к статистике
GRANT EXECUTE ON count_sticker_uses_by_month TO Administarator;
GRANT EXECUTE ON count_messages_specified_month_by_day TO Administarator;
GRANT EXECUTE ON count_messages_specified_year_by_month TO Administarator;
GRANT EXECUTE ON count_messages_by_year TO Administarator;
GRANT EXECUTE ON count_new_users_specified_month_by_day TO Administarator;
GRANT EXECUTE ON count_new_users_specified_year_by_month TO Administarator;
GRANT EXECUTE ON count_new_users_by_year TO Administarator;
GRANT EXECUTE ON count_users_active_in_month_for_all_year TO Administarator;
GRANT EXECUTE ON count_users_by_interface_language TO Administarator;
GRANT EXECUTE ON popular_settings_users TO Administarator;
GRANT EXECUTE ON time_sending_notifications_users_by_hour TO Administarator;
GRANT EXECUTE ON count_messages_written_per_month_by_user TO Administarator;
GRANT EXECUTE ON count_files_sent_and_total_volume_for_month TO Administarator;
GRANT EXECUTE ON count_downloads_user_uploaded_files_per_month TO Administarator;
GRANT EXECUTE ON count_stickers_sent_per_month TO Administarator;
GRANT EXECUTE ON add_users_statistics_for_app TO Administarator;

GRANT SELECT ON Users_statistics_for_app TO Administarator;


-- Пользователь для входа
CREATE LOGIN Login_user WITH PASSWORD = 'login';
CREATE USER Login_user FOR LOGIN Login_user;

GRANT EXECUTE ON get_verify_code TO Login_user;
GRANT EXECUTE ON validate_verify_code TO Login_user;
GRANT EXECUTE ON logout TO Login_user;

GRANT INSERT ON all_base_user TO Login_user;
