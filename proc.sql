﻿-- создание текстового сообщения
CREATE PROCEDURE add_message_text_in_private_chat
    @user_create INT,
	@chat_id INT,
	@text VARCHAR(4096)
AS
BEGIN
	INSERT INTO Text_message(message_text)
		VALUES (@text);
	DECLARE @text_message_id BIGINT, @chat_message_id BIGINT;
	SET @text_message_id = @@IDENTITY;
	INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (GETDATE(), @user_create, 1, @text_message_id, 1, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	UPDATE Text_message
		SET chat_message_id = @chat_message_id
		WHERE text_message_id = @text_message_id;
	INSERT Private_chat_Chat_messages (chat_message_id, private_chat_id)
		VALUES (@chat_message_id, @chat_id);
	RETURN @chat_message_id;
END;
GO

CREATE PROCEDURE add_message_text_in_group_chat
    @user_create INT,
	@chat_id INT,
	@text VARCHAR(4096)
AS
BEGIN
	INSERT INTO Text_message(message_text)
		VALUES (@text);
	DECLARE @text_message_id BIGINT, @chat_message_id BIGINT;
	SET @text_message_id = @@IDENTITY;
	INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (GETDATE(), @user_create, 1, @text_message_id, 0, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	UPDATE Text_message
		SET chat_message_id = @chat_message_id
		WHERE text_message_id = @text_message_id;
	INSERT Group_chat_Chat_messages (chat_message_id, group_chat_id)
		VALUES (@chat_message_id, @chat_id);
	RETURN @chat_message_id;
END;
GO

-- создание сообщения стикира
CREATE PROCEDURE add_message_sticker_in_private_chat
    @user_create INT,
	@chat_id INT,
	@sticker_id BIGINT,
	@caption VARCHAR(255)
AS
BEGIN
	INSERT INTO Sticker_in_Chat_messages (sticker_id, caption) 
		VALUES (@sticker_id, @caption);
	DECLARE @sticker_in_chat_messages_id BIGINT, @chat_message_id BIGINT;
	SET @sticker_in_chat_messages_id = @@IDENTITY;
    INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (GETDATE(), @user_create, 2, @sticker_in_chat_messages_id, 1, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	UPDATE Sticker_in_Chat_messages
		SET chat_message_id = @@IDENTITY
		WHERE sticker_in_chat_messages_id = @sticker_in_chat_messages_id;
	INSERT Private_chat_Chat_messages (chat_message_id, private_chat_id)
		VALUES (@chat_message_id, @chat_id);
	RETURN @chat_message_id;
END;
GO

CREATE PROCEDURE add_message_sticker_in_group_chat
    @user_create INT,
	@chat_id INT,
	@sticker_id BIGINT,
	@caption VARCHAR(255)
AS
BEGIN
	INSERT INTO Sticker_in_Chat_messages (sticker_id, caption) 
		VALUES (@sticker_id, @caption);
	DECLARE @sticker_in_chat_messages_id BIGINT, @chat_message_id BIGINT;
	SET @sticker_in_chat_messages_id = @@IDENTITY;
    INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (GETDATE(), @user_create, 2, @sticker_in_chat_messages_id, 0, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	UPDATE Sticker_in_Chat_messages
		SET chat_message_id = @@IDENTITY
		WHERE sticker_in_chat_messages_id = @sticker_in_chat_messages_id;
	INSERT Group_chat_Chat_messages (chat_message_id, group_chat_id)
		VALUES (@chat_message_id, @chat_id);
	RETURN @chat_message_id;
END;
GO

-- создание пересланного сообщения
CREATE PROCEDURE add_message_forward_in_private_chat
    @user_create INT,
	@chat_id INT,
	@forward_chat_message_id BIGINT
AS
BEGIN
	DECLARE @chat_message_id BIGINT;
    INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (GETDATE(), @user_create, 4, @forward_chat_message_id, 1, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	INSERT Private_chat_Chat_messages (chat_message_id, private_chat_id)
		VALUES (@@IDENTITY, @chat_id);
	RETURN @chat_message_id;
END;
GO

CREATE PROCEDURE add_message_forward_in_group_chat
    @user_create INT,
	@chat_id INT,
	@forward_chat_message_id BIGINT
AS
BEGIN
	DECLARE @chat_message_id BIGINT;
    INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (GETDATE(), @user_create, 4, @forward_chat_message_id, 0, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	INSERT Group_chat_Chat_messages (chat_message_id, group_chat_id)
		VALUES (@@IDENTITY, @chat_id);
	RETURN @chat_message_id;
END;
GO

-- создание сообщения файла
-- 1. создаешь группу с пустым внешним ключем
-- 2. создаешь файлы с внешним ключем на группу
-- 3. вызываешь процедуру
CREATE PROCEDURE add_message_file_group_in_private_chat
    @user_create INT,
	@chat_id INT,
	@file_group_id BIGINT
AS
BEGIN
	DECLARE @chat_message_id BIGINT;
    INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (GETDATE(), @user_create, 3, @file_group_id, 1, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	UPDATE File_group
		SET chat_message_id = @@IDENTITY
		WHERE file_group_id = @file_group_id;
	INSERT Private_chat_Chat_messages (chat_message_id, private_chat_id)
		VALUES (@@IDENTITY, @chat_id);
END;
GO

CREATE PROCEDURE add_message_file_group_in_group_chat
    @user_create INT,
	@chat_id INT,
	@file_group_id BIGINT
AS
BEGIN
	DECLARE @chat_message_id BIGINT;
    INSERT INTO Chat_messages(send_time, user_create, message_type_id, message_id, is_private_chat, chat_id)
		VALUES (GETDATE(), @user_create, 3, @file_group_id, 0, @chat_id);
	SET @chat_message_id = @@IDENTITY;
	UPDATE File_group
		SET chat_message_id = @@IDENTITY
		WHERE file_group_id = @file_group_id;
	INSERT Group_chat_Chat_messages (chat_message_id, group_chat_id)
		VALUES (@chat_message_id, @chat_id);
END;
GO

CREATE PROCEDURE delete_group_chat
    @user INT,
	@group_chat_id INT
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM Group_chat WHERE group_chat_id = @group_chat_id AND creator_user_id = @user;
	IF (@result = 1)
		UPDATE Group_chat SET is_delete = 1 WHERE group_chat_id = @group_chat_id
	ELSE
		RAISERROR('Error: not enough rights to delete group chat',1,1);
END;
GO

CREATE PROCEDURE ban_user_by_message
    @message_type_id TINYINT,
	@message_id BIGINT,
	@blocking_user_id INT,
	@block_type_id TINYINT,
	@block_time_in_days INT
AS
BEGIN
	DECLARE @blocked_user_id INT;
	SELECT @blocked_user_id = user_create FROM Chat_messages WHERE message_id = @message_id AND message_type_id = @message_type_id;

	INSERT INTO [dbo].[Blocked_user]
           ([block_time_in_days]
           ,[blocked_user_id]
           ,[blocking_user_id]
           ,[block_type_id]
           ,[block_date])
     VALUES
           (@block_time_in_days
           ,@blocked_user_id
           ,@blocking_user_id
           ,@block_type_id
           ,GETDATE())
END;
GO

-- возвращает код верификации для пользователя
CREATE PROCEDURE get_verify_code
    @user_phone VARCHAR(15)
AS
BEGIN
	DECLARE @code DECIMAL(6,0), @find INT;
	SELECT @find = COUNT(*) FROM User_main WHERE phone_number = @user_phone
	PRINT(@find)
	IF (@find = 1)
	BEGIN
		SET @code = 100000 + FLOOR(RAND() * 900000);
		DECLARE @id INT;
		MERGE INTO User_verify_code AS Target
		USING (SELECT user_id FROM User_main WHERE phone_number = @user_phone) AS Source
		ON Target.user_id = Source.user_id
		WHEN MATCHED THEN 
			UPDATE SET Target.verify_code = @code
		WHEN NOT MATCHED BY TARGET THEN 
			INSERT (user_id, verify_code) VALUES (Source.user_id, @code);

	END
	ELSE 
	BEGIN
		SET @code = -1;
		DECLARE @msg VARCHAR(128);
		SET @msg = 'Error: user with phone number ' + CAST(@user_phone AS VARCHAR(10)) + ' not found';
		RAISERROR(@msg,1,1);
	END
	SELECT @code AS result;
END;
GO

-- проверка кода верификации для пользователя. Возвращает id роли, 0 - не успешно
CREATE PROCEDURE validate_verify_code
    @user_phone VARCHAR(15),
	@code DECIMAL(6,0)
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = (CASE WHEN COUNT(*) = 1 THEN 1 ELSE 0 END) FROM User_verify_code JOIN User_main ON User_verify_code.user_id = User_main.user_id AND phone_number = @user_phone AND verify_code = @code;
	IF (@result = 0)
		SELECT CAST(-1 AS INT);
	ELSE
	BEGIN
		INSERT INTO User_account_access (date_time, user_id, operation) VALUES (GETDATE(), (SELECT user_id FROM User_main WHERE phone_number = @user_phone), 1);
		SELECT CAST(role_id AS INT), CAST(user_id AS INT) FROM User_main WHERE phone_number = @user_phone;
	END
END;
GO

-- закрытие сессии
CREATE PROCEDURE logout
	@user_phone VARCHAR(15)
AS
BEGIN
	DECLARE @result TINYINT, @user_id INT;
	SELECT @user_id = user_id FROM User_main WHERE phone_number = @user_phone
	SELECT @result = COUNT(a.user_id) FROM
			(SELECT user_id, MAX(operation_id) last_op FROM User_account_access WHERE operation = 1 AND user_id = @user_id GROUP BY user_id) AS a
		LEFT JOIN
			(SELECT user_id, MAX(operation_id) last_op FROM User_account_access WHERE operation = 0 AND user_id = @user_id GROUP BY user_id) AS b
		ON a.user_id = b.user_id
		WHERE  a.last_op > b.last_op OR b.last_op IS NULL

	IF (@result = 1)
		INSERT INTO User_account_access (date_time, user_id, operation) VALUES (GETDATE(), @user_id, 0);
END;
GO

CREATE PROCEDURE get_message_chat
	@is_private BIT,
	@chat_id INT
AS
BEGIN
	SELECT chat_message_id, CASE WHEN edit_time IS NULL THEN send_time ELSE edit_time END AS time, nickname, Message_type.name AS message_type, CASE WHEN text IS NULL THEN ' ' ELSE text END AS text, CASE WHEN options IS NULL THEN ' ' ELSE options END AS options FROM
	(SELECT Chat_messages.*, Text_message.message_text AS text, NULL AS options FROM Chat_messages JOIN Text_message ON is_private_chat = @is_private AND chat_id = @chat_id AND message_type_id = 1 AND message_id = text_message_id
	UNION ALL
	SELECT Chat_messages.*, Sticker_in_Chat_messages.caption, Sticker.image FROM Chat_messages JOIN Sticker_in_Chat_messages ON is_private_chat = @is_private AND chat_id = @chat_id AND message_type_id = 2 AND message_id = sticker_in_chat_messages_id JOIN Sticker ON Sticker.sticker_id = Sticker_in_Chat_messages.sticker_id
	UNION ALL
	SELECT Chat_messages.*, f.caption, f.file_link FROM Chat_messages JOIN (SELECT File_main.file_group_id, caption, file_link FROM File_main JOIN File_group ON File_main.file_group_id = File_group.file_group_id) AS f ON is_private_chat = @is_private AND chat_id = @chat_id AND message_type_id = 1 AND message_id = f.file_group_id
	UNION ALL
	SELECT Chat_messages.*, NULL, NULL FROM Chat_messages WHERE is_private_chat = @is_private AND chat_id = @chat_id AND message_type_id = 4
	) a
	JOIN Message_type 
	ON a.message_type_id = Message_type.message_type_id
	JOIN User_main
	ON User_main.user_id = a.user_create
	WHERE a.is_delete = 0
END;
GO

CREATE PROCEDURE add_user_to_group_chat
	@phone_number VARCHAR(15),
	@chat_id INT
AS
BEGIN
	DECLARE @count_user TINYINT, @user_id INT;
	SELECT @count_user = COUNT(*) FROM User_main WHERE phone_number = @phone_number AND role_id = 1;

	IF (@count_user = 1)
	BEGIN
		SELECT @user_id = user_id FROM User_main WHERE phone_number = @phone_number;
		INSERT INTO Group_chat_participants (group_chat_id, user_id) VALUES (@chat_id, @user_id)
		RETURN 1;
	END
	RETURN -1;
END;
GO

CREATE PROCEDURE delete_message --NEW
	@user_id INT,
	@chat_message_id BIGINT
AS
BEGIN
	UPDATE Chat_messages SET is_delete = 1 WHERE chat_message_id = @chat_message_id AND user_create = @user_id;	
	RETURN (SELECT CASE WHEN @@ROWCOUNT = 1 THEN 1 ELSE -1 END);
END;
GO

CREATE PROCEDURE update_text_message --NEW
	@user_id INT,
	@chat_message_id BIGINT,
	@new_text VARCHAR(4096)
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM Chat_messages WHERE chat_message_id = @chat_message_id AND user_create = @user_id;
	IF (@result = 1)
	BEGIN
		UPDATE Text_message SET message_text = @new_text WHERE chat_message_id = @chat_message_id;	
	END
	RETURN (SELECT CASE WHEN @@ROWCOUNT = 1 THEN 1 ELSE -1 END);
END;
GO