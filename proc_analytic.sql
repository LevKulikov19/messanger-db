﻿-- Создатель стикеров: вывод информации об количестве использований стикеров по месяцам, которые создал пользователь
CREATE PROCEDURE count_sticker_uses_by_month
	@sticker_id INT
AS
BEGIN
	WITH months AS (SELECT value FROM GENERATE_SERIES(1, 12))

	SELECT months.value, COALESCE(counts.count, 0) as count
	FROM months
	LEFT JOIN (
	  SELECT MONTH(send_time) as month, COUNT(*) as count
	  FROM 
	    Chat_messages
	  JOIN
		(SELECT * FROM Sticker_in_Chat_messages WHERE sticker_id = @sticker_id) a
	  ON message_type_id = 2 AND message_id = sticker_in_chat_messages_id
	  GROUP BY MONTH(send_time)
	) counts ON months.value = counts.month
	ORDER BY months.value;
END
GO
-- Администратор: Вывод информации о количестве сообщений на указанный месяц разбитый по дням
CREATE PROCEDURE count_messages_specified_month_by_day
	@year SMALLINT,
	@month TINYINT
AS
BEGIN
	WITH days AS (
		SELECT value FROM GENERATE_SERIES(1, 31)
	)
	SELECT days.value, COALESCE(counts.count, 0) as count
	FROM days
	LEFT JOIN (
	  SELECT DAY(send_time) as day, COUNT(*) as count
	  FROM Chat_messages
	  WHERE YEAR(send_time) = @year AND MONTH(send_time) = @month
	  GROUP BY DAY(send_time)
	) counts ON days.value = counts.day
	WHERE days.value <= (
	  CASE 
		WHEN @month IN (4, 6, 9, 11) THEN 30 
		-- февраль с учетом високосных годов
		WHEN @month = 2 THEN 
		  CASE 
			WHEN (@year % 4 = 0 AND @year % 100 != 0) OR @year % 400 = 0 THEN 29 
			ELSE 28 
		  END
		ELSE 31 
	  END
	)
	ORDER BY days.value;
END
GO

-- Администратор: Вывод информации о количестве сообщений на указанный год разбитый по месяцам
CREATE PROCEDURE count_messages_specified_year_by_month
	@year SMALLINT
AS
BEGIN
	WITH months AS (SELECT value FROM GENERATE_SERIES(1, 12))

	SELECT months.value, COALESCE(counts.count, 0) as count
	FROM months
	LEFT JOIN (
		SELECT MONTH(send_time) as month, COUNT(*) as count
		FROM Chat_messages
		WHERE YEAR(send_time) = @year
		GROUP BY MONTH(send_time)
	) counts ON months.value = counts.month
	ORDER BY months.value;
END
GO

-- Администратор: Вывод информации о количестве сообщений за всё время разбитый по годам
CREATE PROCEDURE count_messages_by_year
AS
BEGIN
	WITH years AS (
		SELECT value FROM 
			GENERATE_SERIES(
				(SELECT YEAR(MIN(send_time)) FROM Chat_messages),
				(SELECT YEAR(MAX(send_time)) FROM Chat_messages)
			) AS year
	)

	SELECT years.value, COALESCE(counts.count, 0) as count
	FROM years
	LEFT JOIN (
		SELECT YEAR(send_time) as year, COUNT(*) as count
		FROM Chat_messages
		GROUP BY YEAR(send_time)
	) counts ON years.value = counts.year
	ORDER BY years.value;
END
GO

-- Администратор: Вывод информации о количестве новых пользователей на указанный месяц разбитый по дням
CREATE PROCEDURE count_new_users_specified_month_by_day
	@year SMALLINT,
	@month TINYINT	
AS
BEGIN
	WITH days AS (
		SELECT value FROM GENERATE_SERIES(1, 31)
	)
	SELECT days.value, COALESCE(counts.value, 0) as count_new
	FROM days
	LEFT JOIN (
		SELECT DAY(registration_date) as day, COUNT(*) as value
		FROM User_main
		WHERE (MONTH(registration_date)=@month) AND (YEAR(registration_date)=@year)
		GROUP BY DAY(registration_date)

	) counts ON days.value = counts.day
	WHERE days.value <= (
	  CASE 
		WHEN @month IN (4, 6, 9, 11) THEN 30 
		-- февраль с учетом високосных годов
		WHEN @month = 2 THEN 
		  CASE 
			WHEN (@year % 4 = 0 AND @year % 100 != 0) OR @year % 400 = 0 THEN 29 
			ELSE 28 
		  END
		ELSE 31 
	  END
	)
	ORDER BY days.value;
END
GO

-- Администратор: Вывод информации о количестве новых пользователей на указанный год разбитый по месяцам
CREATE PROCEDURE count_new_users_specified_year_by_month
	@year SMALLINT	
AS
BEGIN
	WITH months AS (SELECT value FROM GENERATE_SERIES(1, 12))

	SELECT months.value, COALESCE(counts.count, 0) as count
	FROM months
	LEFT JOIN (
		SELECT MONTH(registration_date) as month, COUNT(*) as count
		FROM User_main
		WHERE YEAR(registration_date)=@year
		GROUP BY MONTH(registration_date)
	) counts ON months.value = counts.month
	ORDER BY months.value;
END
GO

-- Администратор: Вывод информации о количестве новых пользователей за всё время разбитый по годам
CREATE PROCEDURE count_new_users_by_year
AS
BEGIN
	WITH years AS (
		SELECT value FROM 
			GENERATE_SERIES(
				(SELECT YEAR(MIN(registration_date)) FROM User_main),
				(SELECT YEAR(MAX(registration_date)) FROM User_main)
			) AS year
	)

	SELECT years.value, COALESCE(counts.count, 0) as count
	FROM years
	LEFT JOIN (
		SELECT YEAR(registration_date) as year, COUNT(*) as count
		FROM User_main
		GROUP BY YEAR(registration_date)
	) counts ON years.value = counts.year
	ORDER BY years.value;
END
GO

-- Администратор: Количество пользователей активных зарегистрированных в данном месяце за все года
CREATE PROCEDURE count_users_active_in_month_for_all_year
	@month TINYINT
AS
BEGIN
	WITH years AS (
		SELECT value FROM 
			GENERATE_SERIES(
				(SELECT YEAR(MIN(registration_date)) FROM User_main),
				(SELECT YEAR(MAX(registration_date)) FROM User_main)
			) AS year
	)
	
	SELECT years.value, COALESCE(counts.count, 0) as count
	FROM years
	LEFT JOIN (
		SELECT YEAR(registration_date) as year, COUNT(*) as count 
		FROM User_main 
		WHERE online_status = 1 AND MONTH(registration_date) = @month
		GROUP BY YEAR(registration_date)
	) counts ON years.value = counts.year
	ORDER BY years.value;
END
GO

-- Администратор: Вывод информации о количестве пользователей в системе по языкам интерфейса
CREATE PROCEDURE count_users_by_interface_language
AS
BEGIN
	SELECT interface_lang.lang_name, COALESCE(counts.count, 0) as count
	FROM (
		SELECT name as lang_name
		FROM Interface_language
	) interface_lang
	LEFT JOIN (
		SELECT name as lang_name, COUNT(name) as count FROM 
			User_main
		JOIN 
			User_settings
		ON  User_main.user_settings_id = User_settings.user_settings_id
		JOIN
			Interface_language
		ON  User_settings.interface_language_id = Interface_language.interface_language_id
		GROUP BY Interface_language.name
	) counts ON interface_lang.lang_name = counts.lang_name
	ORDER BY interface_lang.lang_name;
END
GO

-- Администратор: Вывести популярные комбинации настроек у пользователей
CREATE PROCEDURE popular_settings_users
AS
BEGIN
	SELECT user_settings_id, COUNT(user_settings_id) as count_use_user_settings FROM User_main
	GROUP BY user_settings_id 
	ORDER BY count_use_user_settings DESC
END
GO

-- Администратор: Вывести время отправки уведомлений ползователей сгруппированых по часам
CREATE PROCEDURE time_sending_notifications_users_by_hour
AS
BEGIN
	WITH hours AS (
		SELECT value FROM GENERATE_SERIES(0, 23)
	)
	SELECT hours.value, COALESCE(counts.value, 0) as count
	FROM hours
	LEFT JOIN (
		SELECT DATEPART(hour, send_time) as hour, COUNT(*) as value FROM Notifications GROUP BY DATEPART(hour, send_time)
	) counts ON hours.value = counts.hour
	ORDER BY hours.value;
END
GO

-- Обычный пользователь: Вывод информации о количестве написанных сообщений за указанный месяц и год, группировка по дням 
CREATE PROCEDURE count_messages_written_per_month_by_user
	@month TINYINT,
	@year SMALLINT,
	@user INT
AS
BEGIN
	WITH days AS (
		SELECT value FROM GENERATE_SERIES(1, 31)
	)
	SELECT days.value, COALESCE(counts.count, 0) as count
	FROM days
	LEFT JOIN (
		SELECT DAY(c_time) AS day,COUNT(*) AS count FROM 
		(SELECT user_create, (CASE WHEN edit_time IS NULL THEN send_time ELSE edit_time END) AS c_time FROM Chat_messages) AS a
		WHERE	user_create = @user AND
				MONTH(c_time) = @month AND
				YEAR(c_time) = @year
		GROUP BY DAY(c_time), user_create
	) AS counts ON days.value = counts.day
	WHERE days.value <= (
	  CASE 
		WHEN @month IN (4, 6, 9, 11) THEN 30 
		-- февраль с учетом високосных годов
		WHEN @month = 2 THEN 
		  CASE 
			WHEN (@year % 4 = 0 AND @year % 100 != 0) OR @year % 400 = 0 THEN 29 
			ELSE 28 
		  END
		ELSE 31 
	  END
	)
	ORDER BY days.value;
END
GO

-- Обычный пользователь: Вывод информации о количестве отправленных файлов и их суммарный объем за указанный месяц и год, группировка по дням 
CREATE PROCEDURE count_files_sent_and_total_volume_for_month
	@month TINYINT,
	@year SMALLINT,
	@user INT
AS
BEGIN
	WITH days AS (
		SELECT value FROM GENERATE_SERIES(1, 31)
	)
	SELECT days.value, COALESCE(counts.count, 0) as count, COALESCE(counts.size, 0) as size
	FROM days
	LEFT JOIN (
		SELECT DAY(send_time) AS day, COUNT(*) AS count, SUM(File_main.size) AS size 
		FROM 
			Chat_messages 
			JOIN
			File_group
			ON (Chat_messages.message_type_id=3) AND (Chat_messages.message_id = File_group.file_group_id)
			JOIN
			File_main
			ON (File_group.file_group_id = File_main.file_group_id)
		WHERE	user_create = @user AND
				MONTH(CASE WHEN edit_time IS NULL THEN send_time ELSE edit_time END) = @month AND
				YEAR(CASE WHEN edit_time IS NULL THEN send_time ELSE edit_time END) = @year
		GROUP BY DAY(send_time), user_create
	) AS counts ON days.value = counts.day
	WHERE days.value <= (
	  CASE 
		WHEN @month IN (4, 6, 9, 11) THEN 30 
		-- февраль с учетом високосных годов
		WHEN @month = 2 THEN 
		  CASE 
			WHEN (@year % 4 = 0 AND @year % 100 != 0) OR @year % 400 = 0 THEN 29 
			ELSE 28 
		  END
		ELSE 31 
	  END
	)
	ORDER BY days.value;
END
GO

-- Обычный пользователь: Вывод информации о количестве и их суммарном объеме скачиваний пользователем фалов за указанный месяц и год, группировка по дням 
CREATE PROCEDURE count_downloads_user_uploaded_files_per_month
	@month TINYINT,
	@year SMALLINT,
	@user INT
AS
BEGIN
	WITH days AS (
		SELECT value FROM GENERATE_SERIES(1, 31)
	)
	SELECT days.value, COALESCE(counts.count, 0) as count
	FROM days
	LEFT JOIN (
		SELECT DAY(download_time) AS day, COUNT(*) AS count, COUNT(File_main.size) AS size 
		FROM 
			User_downloaded_file
			JOIN
			File_main
			ON User_downloaded_file.file_id = File_main.file_id
		WHERE	downloading_user_id = @user AND
				MONTH(download_time) = @month AND
				YEAR(download_time) = @year
		GROUP BY DAY(download_time), downloading_user_id
	) AS counts ON days.value = counts.day
	WHERE days.value <= (
	  CASE 
		WHEN @month IN (4, 6, 9, 11) THEN 30 
		-- февраль с учетом високосных годов
		WHEN @month = 2 THEN 
		  CASE 
			WHEN (@year % 4 = 0 AND @year % 100 != 0) OR @year % 400 = 0 THEN 29 
			ELSE 28 
		  END
		ELSE 31 
	  END
	)
	ORDER BY days.value;
END
GO

-- Обычный пользователь: Вывод информации о количестве отправленных стикиров за указанный месяц и год, группировка по дням 
CREATE PROCEDURE count_stickers_sent_per_month
	@month TINYINT,
	@year SMALLINT,
	@user INT
AS
BEGIN
	WITH days AS (
		SELECT value FROM GENERATE_SERIES(1, 31)
	)
	SELECT days.value, COALESCE(counts.count, 0) as count
	FROM days
	LEFT JOIN (
		SELECT DAY(c_time) AS day,COUNT(*) AS count 
		FROM 
			(SELECT user_create, message_type_id, (CASE WHEN edit_time IS NULL THEN send_time ELSE edit_time END) AS c_time FROM Chat_messages) AS a
		WHERE		message_type_id = 2 AND
					user_create = @user AND
					MONTH(c_time) = @month AND
					YEAR(c_time) = @year
		GROUP BY DAY(c_time), user_create
	) AS counts ON days.value = counts.day
	WHERE days.value <= (
	  CASE 
		WHEN @month IN (4, 6, 9, 11) THEN 30 
		-- февраль с учетом високосных годов
		WHEN @month = 2 THEN 
		  CASE 
			WHEN (@year % 4 = 0 AND @year % 100 != 0) OR @year % 400 = 0 THEN 29 
			ELSE 28 
		  END
		ELSE 31 
	  END
	)
	ORDER BY days.value;
END
GO

CREATE PROCEDURE add_users_statistics_for_app
	@collected_by_user INT
AS
BEGIN
	INSERT INTO Users_statistics_for_app
           (number_of_sent_messages
           ,number_of_sent_files
           ,total_volume_of_sent_files
           ,number_of_downloads_of_sent_files
           ,number_of_sent_stickers
           ,user_id
		   ,date_collected)
     VALUES
           ((SELECT COUNT(*) FROM Chat_messages)
           ,(SELECT COUNT(*) FROM Chat_messages WHERE message_type_id=3)
           ,(SELECT SUM(size)/1073741824 FROM File_main)
           ,(SELECT COUNT(*) FROM User_downloaded_file)
           ,(SELECT COUNT(*) FROM Chat_messages WHERE message_type_id=2)
           ,@collected_by_user
		   ,GETDATE())
	RETURN @@IDENTITY
END
GO