﻿-- проверка на то что дата отправки сообщения не больше времени редатирования 
CREATE TRIGGER Chat_messages_send_time_edit_time ON Chat_messages AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM inserted WHERE (edit_time IS NOT NULL) AND NOT(send_time < edit_time);
	IF (@result > 0)
	BEGIN
		RAISERROR('Error: sending time is longer than editing time ',1,1);
		ROLLBACK;
	END
END
GO

-- проверка при добавлении сообщения есть ли создатель сообщения в чате
CREATE TRIGGER Chat_messages_user_create ON Chat_messages AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE inserted_cursor CURSOR FOR
		SELECT chat_message_id, is_private_chat, chat_id, user_create FROM inserted;
	OPEN inserted_cursor;
	DECLARE @chat_message_id BIGINT, @is_private_chat BIT, @chat_id BIGINT, @user_create INT;
	FETCH NEXT FROM inserted_cursor INTO @chat_message_id, @is_private_chat, @chat_id, @user_create;
	DECLARE @msg VARCHAR(128), @result TINYINT;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (@is_private_chat = 0)
			SELECT @result = COUNT(*) FROM Group_chat_participants WHERE (group_chat_id = @chat_id) AND (user_id = @user_create);
		ELSE
			SELECT @result = COUNT(*) FROM Private_chat WHERE (private_chat_id = @chat_id) AND (user_1_id = @user_create OR user_2_id = @user_create);

		IF NOT(@result = 1)
		BEGIN
			SET @msg = 'Error: user with id ' + CAST(@user_create AS VARCHAR(10)) + ' is not in the chat with id ' + CAST(@chat_id AS VARCHAR(10));
			RAISERROR(@msg,1,1);
			ROLLBACK;
		END
		FETCH NEXT FROM inserted_cursor INTO @chat_message_id, @is_private_chat, @chat_id, @user_create;
	END

	CLOSE inserted_cursor
	DEALLOCATE inserted_cursor
END
GO

-- проверка в сообщении внешенго ключа на ресурс
CREATE TRIGGER Chat_messages_message_id_foregin_key ON Chat_messages AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result SMALLINT;
	DECLARE @result_unique SMALLINT;
	DECLARE inserted_cursor CURSOR FOR
		SELECT message_type_id, message_id, chat_message_id FROM inserted
	OPEN inserted_cursor
	DECLARE @message_type_id TINYINT, @message_id BIGINT, @chat_message_id BIGINT
	FETCH NEXT FROM inserted_cursor INTO @message_type_id, @message_id, @chat_message_id
	DECLARE @msg VARCHAR(128);
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (@message_type_id = 4 AND @message_id = @chat_message_id) 
		BEGIN
			SET @msg = 'Error: message forward on self in ' + @chat_message_id;
			RAISERROR(@msg,1,1);
			ROLLBACK;
		END
		
		DECLARE @count_row SMALLINT;
		SELECT @count_row = COUNT(*) FROM Chat_messages;
		PRINT(@count_row)

		IF (@message_type_id = 1)
		BEGIN
			SELECT @result = COUNT(*) FROM Text_message WHERE text_message_id IN (CAST(@message_id AS VARCHAR(100)));
			-- проверка на уникальность
			IF (NOT(@result = 0) AND @count_row<>1)
				SELECT @result_unique = COUNT(*) FROM Chat_messages WHERE message_type_id = 1 AND message_id = @message_id
		END
		ELSE IF (@message_type_id = 3)
		BEGIN
			SELECT @result = COUNT(*) FROM File_group WHERE file_group_id IN (CAST(@message_id AS VARCHAR(100)));
			-- проверка на уникальность
			IF (NOT(@result = 0) AND @count_row<>1)
				SELECT @result_unique = COUNT(*) FROM Chat_messages WHERE message_type_id = 3 AND message_id = @message_id
		END
		ELSE IF (@message_type_id = 2)
		BEGIN
			SELECT @result = COUNT(*) FROM Sticker_in_Chat_messages WHERE sticker_in_chat_messages_id IN (CAST(@message_id AS VARCHAR(100)));
			-- проверка на уникальность
			IF (NOT(@result = 0) AND @count_row<>1)
				SELECT @result_unique = COUNT(*) FROM Chat_messages WHERE message_type_id = 2 AND message_id = @message_id
		END
		ELSE IF (@message_type_id = 4)
		BEGIN
			SELECT @result = COUNT(*) FROM Chat_messages WHERE chat_message_id IN (CAST(@message_id AS VARCHAR(100)));
			-- проверка на уникальность
			IF (NOT(@result = 0) AND @count_row<>1)
				SELECT @result_unique = COUNT(*) FROM Chat_messages WHERE message_type_id = 4 AND message_id = @message_id
		
		END

		IF (@result = 0)
		BEGIN
			SET @msg = 'Error: foregin key message_id in ' + CAST(@chat_message_id AS VARCHAR(100));
			RAISERROR(@msg,1,1);
			ROLLBACK;
		END
		
		IF (@result_unique <> 1)
		BEGIN
		
			SET @msg = 'Error: foregin key not unique message_id in ' + CAST(@chat_message_id AS VARCHAR(100));
			RAISERROR(@msg,2,2);
			ROLLBACK;
		END
		FETCH NEXT FROM inserted_cursor INTO @message_type_id, @message_id, @chat_message_id;
	END

	CLOSE inserted_cursor
	DEALLOCATE inserted_cursor
END
GO

-- проверка в сообщении внешенго ключа на чат
CREATE TRIGGER Chat_messages_chat_id_foregin_key ON Chat_messages AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result BIT;
	DECLARE inserted_cursor CURSOR FOR
		SELECT chat_message_id, is_private_chat, chat_id FROM inserted;
	OPEN inserted_cursor;
	DECLARE @chat_message_id BIGINT, @is_private_chat BIT, @chat_id INT;
	FETCH NEXT FROM inserted_cursor INTO @chat_message_id, @is_private_chat, @chat_id;
	DECLARE @msg VARCHAR(128);
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (@is_private_chat = 0)
		BEGIN
			SELECT @result = COUNT(*) FROM Group_chat WHERE group_chat_id IN (CAST(@chat_id AS VARCHAR(100)));
		END
		ELSE
		BEGIN
			SELECT @result = COUNT(*) FROM Private_chat WHERE private_chat_id IN (CAST(@chat_id AS VARCHAR(100)));
		END

		IF (@result = 0)
		BEGIN
			SET @msg = 'Error: foregin key chat_id in ' + CAST(@chat_message_id AS VARCHAR(100));
			RAISERROR(@msg,2,2);
			ROLLBACK;
		END
		FETCH NEXT FROM inserted_cursor INTO @chat_message_id, @is_private_chat, @chat_id;
	END
	CLOSE inserted_cursor
	DEALLOCATE inserted_cursor
END
GO

-- пользователь не может прочитать сообщение если он не состоит в этом чате или группе
CREATE TRIGGER Messages_read_user_have_read_message ON Messages_read AFTER INSERT, UPDATE
AS
BEGIN
		DECLARE @result BIT;
	DECLARE inserted_cursor CURSOR FOR
		SELECT user_id, is_private_chat, chat_id FROM
			inserted
		JOIN
			Chat_messages
		ON inserted.chat_message_id = Chat_messages.chat_message_id;
	OPEN inserted_cursor;
	DECLARE @user_id INT, @is_private_chat BIT, @chat_id INT;
	FETCH NEXT FROM inserted_cursor INTO @user_id, @is_private_chat, @chat_id;
	DECLARE @msg VARCHAR(128);
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (@is_private_chat = 0)
		BEGIN
			SELECT @result = COUNT(*) FROM Group_chat_participants WHERE group_chat_id = @chat_id AND user_id = @user_id;
		END
		ELSE
		BEGIN
			SELECT @result = COUNT(*) FROM Private_chat WHERE (private_chat_id = @chat_id) AND (user_1_id = @user_id OR user_2_id = @user_id);
		END

		IF (@result = 0)
		BEGIN
			SET @msg = 'Error: user ' + CAST(@user_id AS VARCHAR(10)) + ' does not have the right to read this message';
			RAISERROR(@msg,2,2);
			ROLLBACK;
		END
		FETCH NEXT FROM inserted_cursor INTO @user_id, @is_private_chat, @chat_id;
	END
	CLOSE inserted_cursor
	DEALLOCATE inserted_cursor
END
GO

-- Вход и выход пользователя в аккаунт: у пользователя может быть не более 1 000 оперций в день 
CREATE TRIGGER User_account_access_1000_per_day_max ON User_account_access AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM 
	(SELECT * FROM
		User_account_access
	UNION ALL
		SELECT * FROM inserted) a
	GROUP BY user_id
	HAVING COUNT(user_id) > 1000
	IF (@result <> 0)
	BEGIN
		RAISERROR('Error: user can have no more than 1,000 transactions per day',2,2);
		ROLLBACK;
	END
END
GO

-- Вход и выход пользователя в аккаунт: не может быть значение "Дата и время" меньше значения в поле “Дата регистрации” 
CREATE TRIGGER User_account_access_date_op_date_reg ON User_account_access AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM 
		inserted
	JOIN
		User_main
	ON inserted.user_id = User_main.user_id
	WHERE inserted.date_time <= User_main.registration_date

	IF (@result <> 0)
	BEGIN
		RAISERROR('Error: “Date and time” value cannot be less than the value in the “Registration date” field',2,2);
		ROLLBACK;
	END
END
GO

-- Личный чат: user_1_id и user_2_id не равны
CREATE TRIGGER Private_chat_users ON Private_chat AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM inserted	WHERE user_1_id = user_2_id

	IF NOT(@result = 0)
	BEGIN
		RAISERROR('Error: user_1_id and user_2_id cannot be equal',2,2);
		ROLLBACK;
	END
END
GO

-- Личный чат: для user_1_id и user_2_id существует только 1 чат
CREATE TRIGGER Private_chat_users_unique ON Private_chat AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM Private_chat JOIN inserted 
			 ON (Private_chat.user_1_id = inserted.user_1_id AND Private_chat.user_2_id = inserted.user_2_id)
			 OR (Private_chat.user_1_id = inserted.user_1_id AND Private_chat.user_2_id = inserted.user_2_id)

	IF NOT(@result = 1)
	BEGIN
		RAISERROR('Error: private chat must be unique',2,2);
		ROLLBACK;
	END
END
GO

-- Личный чат: Закрепленное сообщение должно относится к текущему чату
CREATE TRIGGER Private_chat_pin_message ON Private_chat AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM 
		inserted
	JOIN
		Chat_messages
	ON inserted.pin_message = Chat_messages.chat_message_id

	IF NOT(@result = 0)
	BEGIN
		RAISERROR('Error: pinned message does not belong to this chat',2,2);
		ROLLBACK;
	END
END
GO

-- Групповой чат: Максимальное количестов пользователей в 1 группе 200 000
CREATE TRIGGER Group_chat_user_max_200000 ON Group_chat_participants AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM 
		(SELECT * FROM Group_chat_participants
		UNION ALL
		SELECT * FROM inserted) a
	GROUP BY user_id
	HAVING COUNT(user_id) > 1000
	IF NOT(@result = 0)
	BEGIN
		RAISERROR('Error: maximum number of users in 1 group is 200000',2,2);
		ROLLBACK;
	END
END
GO

-- Групповой чат: Закрепленное сообщение должно относится к текущему чату
CREATE TRIGGER Group_chat_pin_message ON Group_chat AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM 
		inserted
	JOIN
		Chat_messages
	ON inserted.pin_message = Chat_messages.chat_message_id

	IF NOT(@result = 0)
	BEGIN
		RAISERROR('Error: pinned message does not belong to this chat',2,2);
		ROLLBACK;
	END
END
GO

-- Сообщение в чате группа файлов: максимальное количество файлов в группе 10
CREATE TRIGGER File_group_count_file_max_10 ON File_main AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;

	SELECT @result = COUNT(*) FROM 
		(SELECT * FROM File_main
		UNION ALL
		SELECT * FROM inserted) a 
	GROUP BY file_group_id
	HAVING COUNT(file_group_id) > 10

	IF NOT(@result = 0)
	BEGIN
		RAISERROR('Error: maximum number of files in a group is 10',2,2);
		ROLLBACK;
	END
END
GO

-- Стикер: cоздателем стикира может быть пользователь с ролью "Создатель стикиров" или "Администратор" 
CREATE TRIGGER Sticker_user_create ON Sticker AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;

	SELECT @result = COUNT(*) FROM 
		inserted
		JOIN
		User_main
	ON inserted.user_create = User_main.user_id
	WHERE NOT(User_main.role_id = 3 OR User_main.role_id = 4)

	IF NOT(@result = 0)
	BEGIN
		RAISERROR('Error: user does not have rights to create stickers',2,2);
		ROLLBACK;
	END
END
GO

-- Заблокированный пользователь: Не может быть дата блокировки меньше значения в поле “Дата регистрации” 
CREATE TRIGGER Blocked_user_block_date_reg_date ON Blocked_user AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM 
		inserted
	JOIN
		User_main
	ON inserted.blocked_user_id = User_main.user_id
	WHERE inserted.block_date <= User_main.registration_date

	IF NOT(@result = 0)
	BEGIN
		RAISERROR('Error: blocking date cannot be less than the value in the “Registration date” field',2,2);
		ROLLBACK;
	END
END
GO

-- Оповещение: Не может быть дата отправки меньше значения в поле “Дата регистрации” 
CREATE TRIGGER Notifications_send_time_reg_date ON Notifications AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @result TINYINT;
	SELECT @result = COUNT(*) FROM 
		inserted
	JOIN
		User_main
	ON inserted.user_id = User_main.user_id
	WHERE inserted.send_time <= User_main.registration_date

	IF NOT(@result = 0)
	BEGIN
		RAISERROR('Error: send date cannot be less than the value in the “Registration date” field',2,2);
		ROLLBACK;
	END
END
GO

-- Закрытие сессии, при генерации нового кода
CREATE TRIGGER User_verify_code_add_new ON User_verify_code AFTER INSERT
AS
BEGIN
	INSERT INTO User_account_access (date_time, user_id, operation)
		SELECT GETDATE(), inserted.user_id, 0 FROM inserted JOIN 
		(SELECT a.user_id FROM
			(SELECT user_id, MAX(operation_id) last_op FROM User_account_access WHERE operation = 1 GROUP BY user_id) AS a
		LEFT JOIN
			(SELECT user_id, MAX(operation_id) last_op FROM User_account_access WHERE operation = 0 GROUP BY user_id) AS b
		ON a.user_id = b.user_id
		WHERE  a.last_op > b.last_op OR b.last_op IS NULL) c
		ON c.user_id = inserted.user_id;
END
GO
