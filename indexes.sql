-- Для процедур с группировкой и фильтрацией по дате/времени отправки сообщения создать индекс по столбцу send_time таблицы Chat_messages:
CREATE INDEX idx_chat_messages_send_time ON Chat_messages(send_time) INCLUDE (edit_time);
GO

-- Для процедур с группировкой по ID пользователя создать индекс по столбцу user_id:
CREATE INDEX idx_chat_messages_user_id ON Chat_messages(user_create);
GO

-- Для процедур с фильтрацией по типу сообщения:
CREATE INDEX idx_chat_messages_message_type ON Chat_messages(message_type_id);
GO

-- Для процедур с фильтрацией по типу сообщения текстовые:
CREATE INDEX idx_chat_messages_message_type_1 ON Chat_messages(message_type_id, message_id)
WHERE message_type_id = 1;
GO

-- Для процедур с фильтрацией по типу сообщения стикиры:
CREATE INDEX idx_chat_messages_message_type_2 ON Chat_messages(message_type_id, message_id)
WHERE message_type_id = 2;
GO

-- Для процедур с фильтрацией по типу сообщения файлы:
CREATE INDEX idx_chat_messages_message_type_3 ON Chat_messages(message_type_id, message_id)
WHERE message_type_id = 3;
GO

-- Для процедур с фильтрацией по типу сообщения пересланные:
CREATE INDEX idx_chat_messages_message_type_4 ON Chat_messages(message_type_id, message_id)
WHERE message_type_id = 4;
GO

-- Для процедур с группировкой по языку интерфейса создать индекс по столбцу interface_language_id в таблице User_settings:
CREATE INDEX idx_user_settings_interface_lang ON User_settings(interface_language_id);
GO
 
-- Для процедур с группировкой по ID загрузившего пользователя создать индекс в таблице User_downloaded_file:
CREATE INDEX idx_user_downloaded_files_user_id ON User_downloaded_file(downloading_user_id);
GO

-- Для процедур работающих с датой регистации пользователей, например count_users_active_in_month_for_all_year
CREATE INDEX idx_user_main_registration_date ON User_main(registration_date) INCLUDE (online_status);
GO

-- Для процедур работающих с датой отправки оповешений, например time_sending_notifications_users_by_hour
CREATE INDEX idx_notifications_send_time ON Notifications(send_time);
GO

-- Для процедур выполняющие расчет размеров файлов в группах, например count_files_sent_and_total_volume_for_month
CREATE INDEX idx_file_main_file_group_id ON File_main(file_group_id) INCLUDE (size);
GO

-- Для процедур выполняющие поиск пользователей, которые скачали файл:
CREATE INDEX idx_user_downloaded_file_file_id ON User_downloaded_file(file_id) INCLUDE (downloading_user_id);
GO